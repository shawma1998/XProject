EventView = BaseView:Extend()

function EventView:New()
	local o = EventView.super.New(self)
	o.abName = "ui_event"
	o.resName = "EventView"
	o.layer = ENUM.UI_LAYER.UI
	o.hide_pre_view = true
	o.curr = 0
	return o
end

function EventView:loadSuccess()
	self.btn = GetNode(self.transform,{"btn_go"},"GameObject")

	self:InitEvent()
end

function EventView:InitEvent()
	local click_func = function (target)
		if target == self.btn then
			self.curr = self.curr + 1
			if self.curr == 3 then
				GlobeEvent:Fire(Events.TEST_EVENT,os.time())
			end
		end
	end
	AddClick(self.btn,click_func)

	print("1111111111111111111111111111")
	for v in npair({1,2,3,4,5,6,7}) do
		print("qqqqqqqqq0------------>",v)
	end
	print("2222222222222222222222222222222")
	for v in npair({1,2,3,4,5,6,7}) do
		print("qqqqqqqqq0------------>",v)
	end

	self.bind_id = self:BindEvent(GlobeEvent,Events.TEST_EVENT,function(arg)
		self:Close()
	end)
end

function EventView:destroy_callback()
end

function EventView:__delete()
end

function npair (tab)
	local i = 1
	return function()
		local val = tab[i]
		i = i + 1
		if i >= 3 then return nil end
		return val
	end
end