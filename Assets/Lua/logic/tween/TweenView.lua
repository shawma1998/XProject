TweenView = BaseView:Extend()

function TweenView:New()
	local o = TweenView.super.New(self)
	o.abName = "ui_tween"
	o.resName = "TweenView"
	o.layer = ENUM.UI_LAYER.UI
	o.hide_pre_view = true
	return o
end

function TweenView:loadSuccess()
	self.node_alpha , self.node_scale , self.node_pos = GetNode(self.transform,{"node_alpha","node_scale","node_pos"},"Transform")

	self:InitEvent()
end

function TweenView:InitEvent()

	local info1 = {
		ani_parent = self.node_alpha,
		t_type = TweenType.ALPHA,
		start = false,
		target = 0,
		time = 2,
		motion_type = TweenMotion.pingPong,
		loop = true
	}
	self.t_id1 = TweenManager:GetInstance():AddTween (self, info1)

	local info2 = {
		ani_parent = self.node_pos,
		t_type = TweenType.POS,
		start = false,
		target = Vector2(300,204),
		time = 2,
		motion_type = TweenMotion.pingPong,
		loop = true
	}
	self.t_id2 = TweenManager:GetInstance():AddTween (self, info2)


	local info3 = {
		ani_parent = self.node_scale,
		t_type = TweenType.SCALE,
		start = false,
		target = Vector3(1.5,1.5,1.5),
		time = 2,
		motion_type = TweenMotion.pingPong,
		loop = true
	}
	self.t_id3 = TweenManager:GetInstance():AddTween (self, info3)


end

function TweenView:destroy_callback()
end

function TweenView:__delete()
	TweenManager:GetInstance():RemoveTween(self.t_id1)
	TweenManager:GetInstance():RemoveTween(self.t_id2)
	TweenManager:GetInstance():RemoveTween(self.t_id3)
end