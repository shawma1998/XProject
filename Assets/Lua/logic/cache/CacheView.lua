CacheView = BaseView:Extend()

function CacheView:New()
	local o = CacheView.super.New(self)
	o.abName = "ui_cache"
	o.resName = "CacheView"
	o.layer = ENUM.UI_LAYER.UI
	o.hide_pre_view = true
	return o
end

function CacheView:loadSuccess()
	self.btn = GetNode(self.transform,{"Button"},"GameObject")
	self.img = GetNode(self.transform,{"Image"},"Image")
	self.labelText = GetNode(self.transform,{"Text"},"Text")
	self.InputField = GetNode(self.transform,{"InputField"},"_InputField")

	self.labelText.text = CookieManager:GetInstance():GetCookie("LOGIN_TEXT") or ""

	local vo = FindVO:New()
	vo:SetData({ x = 2 , y = 3 } , {x = 6 , y = 6})

	self:InitEvent()
end

function CacheView:InitEvent()
	local click_func = function (target)
		if target == self.btn then
			CookieManager:GetInstance():SetCookie("LOGIN_TEXT",self.InputField.text)
			self.labelText.text = CookieManager:GetInstance():GetCookie("LOGIN_TEXT") or ""
		end
	end
	AddClick(self.btn,click_func)
end

function CacheView:destroy_callback()
end

function CacheView:__delete()
end