CardRoomListView = BaseView:Extend()

function CardRoomListView:New()
	local o = CardRoomListView.super.New(self)
	o.abName = "ui_cardgame"
	o.resName = "CardRoomListView"
	o.layer = ENUM.UI_LAYER.UI
	--self.hide_pre_view = true
	return o
end

function CardRoomListView:loadSuccess()
	self.btn_create , self.Content = GetNode(self.transform,{"btn_create","ScrollView/Viewport/Content"},"GameObject")
	self:InitEvent()
	Send(20000,{})
end

function CardRoomListView:InitEvent()

	local click_func = function (target)
		if target == self.btn_create then
			UIManager:GetInstance():LoadView("CardRoomCreateRoomView")
		end
	end
	AddClick(self.btn_create,click_func)

	self.bind_id = self:BindEvent(GlobeEvent,Events.CARD_ROOM_LIST_REFRESH,function(id)
		self:UpdateRoomList()
	end)
end

function CardRoomListView:UpdateRoomList()
    GenItemList(self,"items",self.Content,CardRoomListItem,CardModel:GetInstance():GetRoomList(),function(item,index,data)
		item:SetData(data)
	end)
end

function CardRoomListView:OnClose()
    UIManager:GetInstance():LoadView("MainView")
end

function CardRoomListView:destroy_callback()
end

function CardRoomListView:__delete()
end