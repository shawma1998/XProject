require("logic.cardgame.CardModel")
require("logic.cardgame.CardRoomListView")
require("logic.cardgame.CardRoomListItem")
require("logic.cardgame.CardGameReadyView")
require("logic.cardgame.CardGamePlayView")
require("logic.cardgame.CardRoomCreateRoomView")
require("logic.cardgame.CardGameResultView")
require("logic.cardgame.CardItem")
CardController = BaseClass:Extend()

function CardController:New()
    local o = CardController.super.New(self)
    o:Init()
    return o
end

function CardController:GetInstance()
    if self.instance == nil then
        self.instance = self:New()
    end
    return self.instance
end

function CardController:Init()
    self:BindProto()
    self:BindEvent()
end


function CardController:BindProto()
    NetManager:GetInstance():BindProto(20000,self.Handler20000, self)  --大厅数据
    NetManager:GetInstance():BindProto(20001,self.Handler20001, self)  --创建房间
    NetManager:GetInstance():BindProto(20002,self.Handler20002, self)  --加入房间
    NetManager:GetInstance():BindProto(20003,self.Handler20003, self)  --房间数据更新
    NetManager:GetInstance():BindProto(20004,self.Handler20004, self)  --删除房间
    NetManager:GetInstance():BindProto(20005,self.Handler20005, self)  --等待房间
    NetManager:GetInstance():BindProto(20006,self.Handler20006, self)  --准备
    NetManager:GetInstance():BindProto(20007,self.Handler20007, self)  --游戏数据
    NetManager:GetInstance():BindProto(20008,self.Handler20008, self)  --加牌
    NetManager:GetInstance():BindProto(20009,self.Handler20009, self)  --完牌
    NetManager:GetInstance():BindProto(20010,self.Handler20010, self)  --游戏结束
    NetManager:GetInstance():BindProto(20011,self.Handler20011, self)  --退出房间
end

function CardController:BindEvent()

end


--大厅数据
function CardController:Handler20000(scmd)
    CardModel:GetInstance():InitRoomList(scmd[1])
end

--创建房间
function CardController:Handler20001(scmd)
    if scmd[1] == 1 then
        print("创建成功")
    else
        print("创建失败")
    end
end

function CardController:Handler20002(scmd)
    if scmd[1] == 1 then
        print("创建成功")
    else
        print("创建失败")
    end
end

--房间数据更新
function CardController:Handler20003(scmd)
    CardModel:GetInstance():UpdateRoomDataByID(scmd[4],scmd)
end

--删除房间
function CardController:Handler20004(scmd)
    CardModel:GetInstance():DelRoomByID(scmd[1])
end

--等待房间
function CardController:Handler20005(scmd)
    local player_list = scmd.player_list
    CardModel:GetInstance():SetCurRoomData(player_list)
    UIManager:GetInstance():LoadView("CardGameReadyView")
end

--准备
function CardController:Handler20006(scmd)
    CardModel:GetInstance():UpdateChangeRoomReadyState(scmd[1],scmd[2])
end

--游戏数据
function CardController:Handler20007(scmd)
    CardModel:GetInstance():SetCurRoomCardData(scmd.player_cards)
    UIManager:GetInstance():HideView("CardGameReadyView")
    UIManager:GetInstance():LoadView("CardGamePlayView")
end

--加牌
function CardController:Handler20008(scmd)
    if scmd[1] or scmd.id then
        local id = scmd[1] or scmd.id
        CardModel:GetInstance():AddCard(id,scmd[2])
    end
    EventManager:Fire(Events.UPDATE_CARDS)
end

--完牌
function CardController:Handler20009(scmd)
    CardModel:GetInstance():UpdateEndCardState(scmd[1],true)
end

--游戏结束
function CardController:Handler20010(scmd)
    local result = CardModel:GetInstance():CheckGameResult(scmd[1] , scmd[2])
    UIManager:GetInstance():HideView("CardGamePlayView")
    local view = UIManager:GetInstance():LoadView("CardGameResultView")
    view:SetData(result)
end

--推出房间
function CardController:Handler20011(scmd)
    if scmd[1] == 1 then
        CardModel:GetInstance():CleanCurRoomData()
        UIManager:GetInstance():HideView("CardGameReadyView")
    end
end

CardController:GetInstance()



