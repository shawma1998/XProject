CardItem = BaseItem:Extend()

function CardItem:New(...)
    local o = CardItem.super.New(self,...)
    o.abName = "ui_cardgame"
    o.resName = "CardItem"
    o:Load()
    return o
end

function CardItem:loadSuccess()
    self.face , self.obj_bg = GetNode(self.transform,{"face","obj_bg",},"GameObject")
    self.img_color = GetNode(self.transform,{"face/img_color"},"Image")
    self.label_num = GetNode(self.transform,{"face/label_num"},"Text")

    self:InitEvent()
    if self.needRefresh then
        self:SetData(self.data)
    end
end

function CardItem:SetData(data)
    self.data = data
    if self.loaded then
        self.needRefresh = false
        if not self.data then
            self.obj_bg:SetActive(true)
            self.face:SetActive(false)
        else
            self.obj_bg:SetActive(false)
            self.face:SetActive(true)
            local color , num , color_str = CardModel:GetInstance():ConvertNumToCard(self.data)

            ResManager:GetInstance():LoadSprite(self,self.img_color,"ui_cardgame","color_"..color,false)
            self.label_num.text = "<color="..color_str..">" .. num .. "</color>"
        end
    else
        self.needRefresh = true
    end
end

function CardItem:InitEvent ()

end

function CardItem:__delete()

end
