CardGamePlayView = BaseView:Extend()

function CardGamePlayView:New()
	local o = CardGamePlayView.super.New(self)
	o.abName = "ui_cardgame"
	o.resName = "CardGamePlayView"
	o.layer = ENUM.UI_LAYER.UI
	--self.hide_pre_view = true
	return o
end

function CardGamePlayView:loadSuccess()
	self.btn_add , self.btn_end , self.obj_other_end = GetNode(self.transform,{"btn_add","btn_end","obj_other_end"},"GameObject")
	self.img = GetNode(self.transform,{"Image"},"Image")
	self.node_other_cards , self.node_mine_cards = GetNode(self.transform,{"node_other_cards","node_mine_cards"})

	self:InitEvent()
end

function CardGamePlayView:InitEvent()
	local click_func = function (target)
		if target == self.btn_add then
			if #self.mines >= 5 then
				return
			end
			Send(20008)
		elseif target == self.btn_end then
			Send(20009)
		end
	end
	AddClick(self.btn_add,click_func)
	AddClick(self.btn_end,click_func)


	self.bind_id = self:BindEvent(GlobeEvent,Events.UPDATE_CARDS,function(arg)
		self:UpdateView()
	end)

end

function CardGamePlayView:OnShow()
	self:UpdateView()
end

function CardGamePlayView:UpdateView()

	local others = CardModel:GetInstance():GetCurRoomData().other_cards
	self.mines = CardModel:GetInstance():GetCurRoomData().mine_cards

	local end_state = CardModel:GetInstance():GetCurRoomData().end_state
	end_state = end_state or {}
	self.obj_other_end:SetActive(end_state[others.id])
	self.btn_add:SetActive(not end_state[self.mines.id])
	self.btn_end:SetActive(not end_state[self.mines.id])


	if CardModel:GetInstance().curr_is_playing then
		others = #others
	end
	local other_cards = {}
	if type(others) == "number" then
		for i = 1, others do
			table.insert(other_cards,false)
		end
	else
		other_cards = others
	end
	GenItemList(self,"items_mine",self.node_mine_cards,CardItem,self.mines,function(item,index,data)
		item:SetData(data)
	end)
	GenItemList(self,"items_other",self.node_other_cards,CardItem,other_cards,function(item,index,data)
		item:SetData(data)
	end)

end

function CardGamePlayView:destroy_callback()
end

function CardGamePlayView:__delete()
end