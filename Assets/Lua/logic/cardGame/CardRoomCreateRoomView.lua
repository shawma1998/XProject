CardRoomCreateRoomView = BaseView:Extend()

function CardRoomCreateRoomView:New()
	local o = CardRoomCreateRoomView.super.New(self)
	self.abName = "ui_cardgame"
	self.resName = "CardRoomCreateRoomView"
	self.layer = ENUM.UI_LAYER.TOP
	--self.hide_pre_view = true
	return o
end

function CardRoomCreateRoomView:loadSuccess()
	self.btn_create = GetNode(self.transform,{"btn_create"},"GameObject")
	self.InputField = GetNode(self.transform,{"InputField"},"_InputField")
	self.Image = GetNode(self.transform,{"Image"},"Image")
	ResManager:GetInstance():LoadSprite(self,self.Image,"ui_cardgame","color_".. "1",false)
	self:InitEvent()
end

function CardRoomCreateRoomView:InitEvent()
	local click_func = function (target)
		if target == self.btn_create then
			if self.InputField.text == "" then return end
			Send(20001,{self.InputField.text})
			self:Close()
		end
	end
	AddClick(self.btn_create,click_func)
end

function CardRoomCreateRoomView:destroy_callback()
end

function CardRoomCreateRoomView:__delete()
end