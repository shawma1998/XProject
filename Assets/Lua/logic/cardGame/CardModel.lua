CardModel = BaseClass:Extend()

function CardModel:New()
    local o = CardController.super.New(self)
    o:Init()
    return o
end

function CardModel:Init()
    self.room_list = {}
    self.curr_room_data = {}    --当前玩家房间数据
end


function CardModel:GetInstance()
    if self.instance == nil then
        self.instance = self:New()
    end
    return self.instance
end

function CardModel:InitRoomList(rooms)
    self.room_list = rooms
    table.sort(self.room_list,function(a,b)
        return a[4] > b[4]
    end)
    EventManager:Fire(Events.CARD_ROOM_LIST_REFRESH)
end

function CardModel:GetRoomList()
    return self.room_list
end

function CardModel:DelRoomByID(id)
    local index
    for i, v in ipairs(self.room_list) do
        if v[4] == id then
            index = i
        end
    end
    if index then
        table.remove(self.room_list,index)
    end
    EventManager:Fire(Events.CARD_ROOM_LIST_REFRESH)
end

function CardModel:UpdateRoomDataByID(id,scmd)
    for i, v in ipairs(self.room_list) do
        if v[4] == id then
            for i2, v2 in pairs(scmd) do
                if v[i2] then
                    v[i2] = v2
                end
            end
        end
    end
    EventManager:Fire(Events.CARD_ROOM_LIST_REFRESH,scmd[4],scmd)
end

function CardModel:SetCurRoomData(data)
    self.curr_room_data = data
end

function CardModel:GetCurRoomData()
    return self.curr_room_data
end

--清理当前房间数据
function CardModel:CleanCurRoomData()
    self.curr_room_data = {}
end

function CardModel:UpdateChangeRoomReadyState( player_id , action)
    player_id = player_id or PlayerInfo:GetInstance():GetID()
    for i, v in pairs(self.curr_room_data) do
        if (v.id or v[2]) == player_id then
            v[3] = action
        end
    end
    EventManager:Fire(Events.UPDATE_READY_ROOM_DATA)
end

--完牌了哦
function CardModel:UpdateEndCardState( player_id , action)
    player_id = player_id or PlayerInfo:GetInstance():GetID()
    if not self.curr_room_data.end_state then
        self.curr_room_data.end_state = {}
        self.curr_room_data.end_state[player_id] = action
    end
    EventManager:Fire(Events.UPDATE_CARDS)
end

function CardModel:SetCurRoomCardData(player_cards)
    local myId = PlayerInfo:GetInstance():GetID()
    local mine , other = {} , {}
    local mine_id , other_id
    for i, v in pairs(player_cards) do
        local cards = v[1]
        if v and v.id == myId then
            mine = cards
            mine.id = v.id
        else
            other = cards
            other.id = v.id
        end
    end
    self.curr_room_data.mine_cards = mine

    self.curr_room_data.other_cards = other
    self.curr_is_playing = true
    EventManager:Fire(Events.UPDATE_READY_ROOM_DATA)
end

function CardModel:AddCard(id , num)
    if id == PlayerInfo:GetInstance():GetID() then
        table.insert(self.curr_room_data.mine_cards,num)
    else
        table.insert(self.curr_room_data.other_cards,num)
    end
end

local card_name = {
    [1] = "A",[11] = "J" , [12] = "Q" , [13] = "K"
}
local color_convert = {
    [1] = "#ff0000",[2] = "#000000",[3] = "#000000",[4] = "#ff0000",
}
--把卡id转成扑克牌
function CardModel:ConvertNumToCard(num)
    local color = math.ceil(num / 13)
    local card_num = num % 13
    card_num = card_num == 0 and 13 or card_num
    if card_name[card_num] then
        card_num = card_name[card_num]
    else
        card_num = tostring(card_num)
    end
    return color or 0 , card_num or 0 , color_convert[color] or "#000000"
end


--结算游戏
function CardModel:CheckGameResult(mine_cards , other_cards)


    local min_num = 16      --最小牌
    local max_num = 21      --最小牌
    local result = 1        --是否赢了
    local mine = self:GetCardValue(mine_cards)
    local other = self:GetCardValue(other_cards)
    print("mine ==>  ",mine," other ==>> " ,other)
    local function _check_in_save_range (num)
        return num >= min_num and num <= max_num
    end


    --我大于16 且对方大于16 且都没爆点
    if _check_in_save_range(mine) and _check_in_save_range(other) then
        result = mine > other and 1 or -1
    end
    if (mine < min_num or mine > max_num) and _check_in_save_range(other) then
        return -1
    end
    if not _check_in_save_range(mine) and not _check_in_save_range(other) then
        return mine < other and 1 or -1
    end
    if mine == other then
        return 0
    end
end

function CardModel:GetCardValue(cards)
    local res = 0
    for i, v in ipairs(cards) do
        local card_num = v % 13
        card_num = card_num == 0 and 13 or card_num
        res = res + v
    end
    return res
end


