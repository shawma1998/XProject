CardGameResultView = BaseView:Extend()

function CardGameResultView:New()
	local o = CardGameResultView.super.New(self)
	o.abName = "ui_cardgame"
	o.resName = "CardGameResultView"
	o.layer = ENUM.UI_LAYER.UI
	--self.hide_pre_view = true
	return o
end

function CardGameResultView:loadSuccess()
	self.label_result = GetNode(self.transform,{"label_result"},"Text")

	if self.needRefresh then
		self:SetData(self.result)
	end
	self:InitEvent()
end

function CardGameResultView:InitEvent()
end

function CardGameResultView:destroy_callback()
end

function CardGameResultView:__delete()
end

local str = {
	[1] = "你赢了",
	[-1] = "你输了，臭XX",
	[0] = "平手"
}
function CardGameResultView:SetData(result)
	self.result = result
	if self.loaded then
		self.needRefresh = false
		self.label_result.text = str[self.result] or ""
	else
		self.needRefresh = true
	end
end