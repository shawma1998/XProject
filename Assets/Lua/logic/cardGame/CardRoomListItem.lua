CardRoomListItem = BaseItem:Extend()

function CardRoomListItem:New(...)
    local o = CardRoomListItem.super.New(self,...)
    o.abName = "ui_cardgame"
    o.resName = "CardRoomListItem"
    o:Load()
    --self.hide_pre_view = true
    return o
end

function CardRoomListItem:loadSuccess()
    self.label_name , self.label_count = GetNode(self.transform,{"label_name","label_count",},"Text")
    self.btn_join = GetNode(self.transform,{"btn_join"},"GameObject")

    self:InitEvent()
    if self.needRefresh then
        self:SetData(self.data)
    end
end

function CardRoomListItem:SetData(data)
    self.data = data
    if self.loaded then
        self.needRefresh = false
        self.id = self.data[4]
        self.label_count.text = self.data[2] .. "/" .. self.data[3]
        self.label_name.text = self.data[4] .. " : " .. self.data[1]
    else
        self.needRefresh = true
    end
end

function CardRoomListItem:InitEvent ()
    local click_func = function (target)
        Send(20002,{self.id})
    end
    AddClick(self.btn_join,click_func)


    self.bind_id = self:BindEvent(GlobeEvent,Events.CARD_ROOM_LIST_REFRESH,function(id,data)
        if id == self.id then
            self:SetData(data)
        end
    end)

end

function CardRoomListItem:__delete()

end
