CardGameReadyView = BaseView:Extend()

function CardGameReadyView:New()
	local o = CardGameReadyView.super.New(self)
	o.abName = "ui_cardgame"
	o.resName = "CardGameReadyView"
	o.layer = ENUM.UI_LAYER.UI
	--self.hide_pre_view = true
	return o
end

function CardGameReadyView:loadSuccess()
	self.btn_action,
	self.btn_close,
	self.obj_ready_other,
	self.obj_ready_mine
	= GetNode(self.transform,{
		"btn_action",
		"btn_close",
		"obj_ready_other",
		"obj_ready_mine",
	},"GameObject")
	self.img_mine , self.img_other = GetNode(self.transform,{"img_mine","img_other"},"Image")
	self.label_mine,self.label_other,self.label_action = GetNode(self.transform,{"label_mine","label_other","btn_action/label_action"},"Text")

	self:InitEvent()
end

function CardGameReadyView:OnShow()
	self:UpdateView()
end

function CardGameReadyView:UpdateView()
	self.data = CardModel:GetInstance():GetCurRoomData()
	self.id = PlayerInfo:GetInstance():GetID()
	self.label_mine.text = PlayerInfo:GetInstance():GetName()
	local other
	for i, v in ipairs(self.data) do
		if v then
			if (v.id or v[2]) ~= self.id then
				other = v
			else
				self.action = v.action or v[3]
				self.label_action.text = self.action == 1 and "取消准备" or "准备"
				self.obj_ready_mine:SetActive(v[3] == 1)
			end
		end
	end
	if other then
		self.label_other.text = other.name or other[1]
		self.obj_ready_other:SetActive(other[3] == 1)
	else
		self.label_other.text = "空空如也"
		self.obj_ready_other:SetActive(false)
	end
end

function CardGameReadyView:InitEvent()
	AddClick(self.btn_action,function ()
		Send(20006,{
			action = self.action == 1 and 0 or 1
		})
	end)

	self.bind_id = self:BindEvent(GlobeEvent,Events.UPDATE_READY_ROOM_DATA,function(id)
		self:UpdateView()
	end)
end

function CardGameReadyView:destroy_callback()
end

function CardGameReadyView:OnClose()
	Send(20011,{})
end