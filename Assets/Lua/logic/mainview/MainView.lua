MainView = BaseView:Extend()

function MainView:New()
	local o = MainView.super.New(self)
	o.abName = "ui_main"
	o.resName = "MainView"
	o.layer = ENUM.UI_LAYER.MAIN
    o.hide_pre_view = true
	return o
end

function MainView:loadSuccess()
	self.btn_cache , self.btn_res , self.btn_timer , self.btn_ui , self.btn_tween , self.btn_event , self.btn_pool , self.btn_find , self.btn_game = GetNode(self.transform,
            {
                "Scroll/Viewport/Content/btn_cache",
                "Scroll/Viewport/Content/btn_res",
                "Scroll/Viewport/Content/btn_timer",
                "Scroll/Viewport/Content/btn_ui",
                "Scroll/Viewport/Content/btn_tween",
                "Scroll/Viewport/Content/btn_event",
                "Scroll/Viewport/Content/btn_pool",
                "Scroll/Viewport/Content/btn_find",
                "Scroll/Viewport/Content/btn_game",
            },"GameObject")

	self:InitEvent()
end

function MainView:InitEvent()
    local click_func = function (target)
        if target == self.btn_cache then
            UIManager:GetInstance():LoadView("CacheView")
        elseif target == self.btn_res then
        UIManager:GetInstance():LoadView("ResLoadView")
        elseif target == self.btn_timer then
            UIManager:GetInstance():LoadView("TimerView")
        elseif target == self.btn_ui then
            local cfg = LuaMemManager.LoadConfig("ConfigTest")
            UIManager:GetInstance():LoadView("LoginView")
        elseif target == self.btn_tween then
        UIManager:GetInstance():LoadView("TweenView")
        elseif target == self.btn_event then
            UIManager:GetInstance():LoadView("EventView")
        elseif target == self.btn_pool then
            UIManager:GetInstance():LoadView("PoolTestView")
        elseif target == self.btn_find then
            UIManager:GetInstance():LoadView("FindView")
        elseif target == self.btn_game then
            UIManager:GetInstance():LoadView("CardRoomListView")
        end
    end
    AddClick(self.btn_cache,click_func)
    AddClick(self.btn_res,click_func)
    AddClick(self.btn_timer,click_func)
    AddClick(self.btn_ui,click_func)
    AddClick(self.btn_tween,click_func)
    AddClick(self.btn_event,click_func)
    AddClick(self.btn_pool,click_func)
    AddClick(self.btn_find,click_func)
    AddClick(self.btn_game,click_func)
end

function MainView:destroy_callback()
end

function MainView:__delete()
end