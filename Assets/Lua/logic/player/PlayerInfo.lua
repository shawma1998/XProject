PlayerInfo = BaseClass:Extend()

function PlayerInfo:New()
    local o = CardController.super.New(self)
    o:Init()
    return o
end

function PlayerInfo:Init()
    self.info = {
        money           = 0,
        id              = 0,
        name            = "",
        address         = "",
    }
end

function PlayerInfo:SetInfoList(scmd)
    for i, v in pairs(scmd) do
        if self.info[i] then
            self.info[i] = v
            EventManager:Fire(Events.PLAYER_INFO_CHANGED,i)
        end
    end
end

function PlayerInfo:SetInfo(key,val)
    if self.info[key] then
        self.info[key] = val
        EventManager:Fire(Events.PLAYER_INFO_CHANGED,key,val)
    end
end

function PlayerInfo:GetInfo(key)
    return self.info[key] or 0
end


function PlayerInfo:GetID()
    return tonumber(self.info.id)
end

function PlayerInfo:GetName()
    return self.info.name
end

function PlayerInfo:GetInstance()
    if self.instance == nil then
        self.instance = self:New()
    end
    return self.instance
end


