PoolTestItem = BaseItem:Extend()

function PoolTestItem:New(...)
    local o = PoolTestItem.super.New(self,...)
    o.abName = "ui_pool"
    o.resName = "PoolTestItem"
    o:Load()
    --self.hide_pre_view = true
    return o
end

function PoolTestItem:loadSuccess()

    self.labelText = GetNode(self.transform,{"Text"},"Text")

    self:InitEvent()
    if self.needRefresh then
        self:SetData(self.data)
    end
end

function PoolTestItem:SetData(data)
    self.data = data
    if self.loaded then
        self.needRefresh = false
        self.labelText.text = self.data
        self.timer_1 = TimerQuest:GetInstance():AddDelay(function()
            PoolManager:GetInstance():SetObject("PoolTestItem",self)
        end,3)
    else
        self.needRefresh = true
    end
end

function PoolTestItem:InitEvent ()

end

function PoolTestItem:Release()
    ResManager:GetInstance():ClearAllRef(self)
    TimerManager:GetInstance():ClearTimer(self.timer_1)
end
