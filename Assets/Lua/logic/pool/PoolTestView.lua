PoolTestView = BaseView:Extend()

function PoolTestView:New()
    local o = PoolTestView.super.New(self)
    o.abName = "ui_pool"
    o.resName = "PoolTestView"
    o.layer = ENUM.UI_LAYER.UI
    o.hide_pre_view = true
    o.list_items = {}
    return o
end

function PoolTestView:loadSuccess()
    self.btn_add , self.btn_reduce = GetNode(self.transform,{"btn_add","btn_reduce"},"GameObject")
    self.node = GetNode(self.transform,{"node"})
    self:InitEvent()
end

function PoolTestView:InitEvent()
    local click_func = function (target)
        if target == self.btn_add then
            --增加
           -- ResManager:GetInstance():LoadOutSideSprite(self,self.img_out,nil,"big_1",false)
            local item = PoolManager:GetInstance():GetObject("PoolTestItem",self.node)
            item:SetData(os.time())
            item:SetPosition(math.random(-300,300),math.random(-300,300))
        elseif target == self.btn_reduce then
        end
    end
    AddClick(self.btn_add,click_func)
    AddClick(self.btn_reduce,click_func)
end

function PoolTestView:destroy_callback()
end

function PoolTestView:__delete()
end