LoginView2 = BaseView:Extend()

function LoginView2:New()
	local o = LoginView2.super.New(self)
	o.abName = "ui_login"
	o.resName = "LoginView2"
	o.layer = ENUM.UI_LAYER.TOP
	o.hide_pre_view = true
	return o
end

function LoginView2:loadSuccess()
	self.btn_open , self.btn_load = GetNode(self.transform,{"btn_open","btn_load"},"GameObject")
	self.node_item = GetNode(self.transform,{"node_item"},"Transform")
	self:InitEvent()
end

function LoginView2:InitEvent()
	local click_func = function (target)
		if target == self.btn_open then
			UIManager:GetInstance():LoadView("LoginView3")
		elseif target == self.btn_load then
			if self.item then
				self.item:DeleteMe()
				self.item = nil
			else
				self.item = self.item or LoginItem:New(self.node_item)
				self.item:SetData(self.resName)
				self.item:SetPosition(30,30)
			end
		end
	end
	AddClick(self.btn_open,click_func)
	AddClick(self.btn_load,click_func)
end


function LoginView2:destroy_callback()
	if self.item then
		self.item:DeleteMe()
	end
end

function LoginView2:__delete()
end
