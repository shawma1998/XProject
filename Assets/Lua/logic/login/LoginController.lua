LoginController = LoginController or {}

function LoginController:Init()
    self:BindProto()
    self:BindEvent()
end

function LoginController:BindProto()
    NetManager:GetInstance():BindProto(10000,self.Handler10000 , self)
    --NetManager:GetInstance():BindProto(10001,self.Handler10001 , self)
    NetManager:GetInstance():BindProto(10002,self.Handler10002 , self)
end

function LoginController:BindEvent()

end

function LoginController:Handler10000(data)
    if data.code == 1 then
        UIManager:GetInstance():HideView("AccountView")
        UIManager:GetInstance():LoadView("CardRoomListView")
    else
        error("登录失败")
    end
end

function LoginController:Handler10002(data)
    PlayerInfo:GetInstance():SetInfoList(data)
end


LoginController:Init()
return LoginController