AccountView = BaseView:Extend()

function AccountView:New()
	local o = AccountView.super.New(self)
	o.abName = "ui_login"
	o.resName = "AccountView"
	o.layer = ENUM.UI_LAYER.TOP
	--self.hide_pre_view = true
	return o
end

function AccountView:loadSuccess()
	self.ip_list = {"43.138.141.127:8885","127.0.0.1:8885","192.168.31.80:8885"}
	self.label_version = GetNode(self.transform,{"label_version"},"Text")
	self.Button = GetNode(self.transform,{"Button"},"GameObject")
	self.if_account = GetNode(self.transform,{"if_account"},"_InputField")
	self.if_Ip = GetNode(self.transform,{"InputField"},"_InputField")
	self.Dropdown = GetNode(self.transform,{"Dropdown"},"_Dropdown")


	local save_ip = CookieManager:GetInstance():GetCookie("ip")
	if save_ip then
		self.if_Ip.text = save_ip
	end

	self:InitEvent()
end

function AccountView:InitEvent()
	AddClick(self.Button,function ()
		if NetManager.connect then
			self:Login()
			return
		end
		local index = self.Dropdown.value + 1
		local address = self.ip_list[index]
		if self.if_Ip.text ~= "" then
			address = self.if_Ip.text
			CookieManager:GetInstance():SetCookie("ip",address)
		end
		address = Split(address,":")
		NetManager:GetInstance():Connect(address[1],address[2])
	end)


	local function connect_success()
		self:Login()
	end
	self.bind_id = EventManager:Bind(Events.CONNECT_SUCCESS,connect_success)
end

function AccountView:Login()
	if self.if_account.text == "" then return end
	Send(10000,{
		id = self.if_account.text
	})
end

function AccountView:destroy_callback()

	GlobeEvent:UnBind(self.bind_id)
end

function AccountView:__delete()
end