LoginItem = BaseItem:Extend()

function LoginItem:New(...)
    local o = LoginItem.super.New(self,...)
    o.abName = "ui_login"
    o.resName = "LoginItem"
    o:Load()
    --self.hide_pre_view = true
    return o
end

function LoginItem:loadSuccess()
    self.labelText = GetNode(self.transform,{"Text"},"Text")

    self:InitEvent()
    if self.needRefresh then
        self:SetData(self.data)
    end
end

function LoginItem:SetData(data)
    self.data = data
    if self.loaded then
        self.needRefresh = false
        self.labelText.text = "这是动态加载的组件->来自"..self.data
    else
        self.needRefresh = true
    end
end

function LoginItem:InitEvent ()

end

function LoginItem:__delete()
    TimerManager:GetInstance():ClearTimer(self.timer_1)
end
