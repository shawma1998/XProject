LoginView3 = BaseView:Extend()

function LoginView3:New()
	local o = LoginView3.super.New(self)
	o.abName = "ui_login"
	o.resName = "LoginView3"
	o.layer = ENUM.UI_LAYER.TOP
	o.hide_pre_view = true
	return o
end

function LoginView3:loadSuccess()
	self.btn_open , self.btn_load = GetNode(self.transform,{"btn_open","btn_load"},"GameObject")
	self.node_item = GetNode(self.transform,{"node_item"},"Transform")
	self:InitEvent()
end

function LoginView3:InitEvent()
	local click_func = function (target)
		if target == self.btn_open then
			UIManager:GetInstance():LoadView("LoginView")
		elseif target == self.btn_load then
			if self.item then
				self.item:DeleteMe()
				self.item = nil
			else
				self.item = self.item or LoginItem:New(self.node_item)
				self.item:SetData(self.resName)
				self.item:SetPosition(30,30)
			end
		end
	end
	AddClick(self.btn_open,click_func)
	AddClick(self.btn_load,click_func)
end


function LoginView3:destroy_callback()
	if self.item then
		self.item:DeleteMe()
	end
end

function LoginView3:__delete()
end
