LoginView = BaseView:Extend()

function LoginView:New()
	local o = LoginView.super.New(self)
	o.abName = "ui_login"
	o.resName = "LoginView"
	o.layer = ENUM.UI_LAYER.TOP
	o.hide_pre_view = true
	return o
end

function LoginView:loadSuccess()
	self.btn_open , self.btn_load = GetNode(self.transform,{"btn_open","btn_load"},"GameObject")
	self.node_item = GetNode(self.transform,{"node_item"},"Transform")

	self:InitEvent()
end

function LoginView:InitEvent()
	local click_func = function (target)
		if target == self.btn_open then
			UIManager:GetInstance():LoadView("LoginView2")
		elseif target == self.btn_load then
			if self.item then
				self.item:DeleteMe()
				self.item = nil
			else
				self.item = LoginItem:New(self.node_item)
				self.item:SetData(self.resName)
				self.item:SetPosition(30,30)
			end
		end
	end
	AddClick(self.btn_open,click_func)
	AddClick(self.btn_load,click_func)
end

function LoginView:destroy_callback()
	if self.item then
		self.item:DeleteMe()
	end
end

function LoginView:__delete()
end