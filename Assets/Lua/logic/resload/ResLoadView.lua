ResLoadView = BaseView:Extend()

function ResLoadView:New()
	local o = ResLoadView.super.New(self)
	o.abName = "ui_resload"
	o.resName = "ResLoadView"
	o.layer = ENUM.UI_LAYER.UI
	o.hide_pre_view = true
	return o
end

function ResLoadView:loadSuccess()
	self.btn_out , self.btn_ui = GetNode(self.transform,{"btn_out","btn_ui"},"GameObject")
	self.img_out , self.img_ui = GetNode(self.transform,{"img_out","img_ui"},"Image")
	Send(20000,{msg = "hello world"})
	--ResManager:GetInstance():LoadSprite(self,self.img,"ui_login","icon1",false,loadfunc)
	--ResManager:GetInstance():LoadOutSideSprite(self,self.img,nil,"big_1",false,loadfunc)
	self:InitEvent()
end

function ResLoadView:InitEvent()
	local click_func = function (target)
		if target == self.btn_out then
			ResManager:GetInstance():LoadOutSideSprite(self,self.img_out,nil,"big_1",false)
		elseif target == self.btn_ui then
			ResManager:GetInstance():LoadSprite(self,self.img_ui,"ui_login","Bigbar",false)
		end
	end
	AddClick(self.btn_out,click_func)
	AddClick(self.btn_ui,click_func)
end

function ResLoadView:destroy_callback()
end

function ResLoadView:__delete()
end