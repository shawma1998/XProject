TimerView = BaseView:Extend()

function TimerView:New()
	local o = TimerView.super.New(self)
	o.abName = "ui_timer"
	o.resName = "TimerView"
	o.layer = ENUM.UI_LAYER.TOP
	o.hide_pre_view = true
	return o
end

function TimerView:loadSuccess()
	self.btn = GetNode(self.transform,{"btn_go"},"GameObject")
	self.node_item = GetNode(self.transform,{"node_item"},"Transform")
	self.label_cd = GetNode(self.transform,{"label_cd"},"Text")
	self.InputField = GetNode(self.transform,{"InputField"},"_InputField")

	self:InitEvent()
end

function TimerView:InitEvent()
	local click_func = function (target)
		if target == self.btn then
			self:ClearTimer()
			if self.InputField.text == "" or not tonumber(self.InputField.text) then return end
			local time = tonumber(self.InputField.text)
			if self.item then
				self.item:DeleteMe()
			end

			self.timer_1 = TimerQuest:GetInstance():AddDelay(function()
				self.item = self.item or LoginItem:New(self.node_item)
				self.item:SetData(self.resName)
			end,time)

			local _time = 0
			self.timer_2 = TimerQuest:GetInstance():AddRepeat(function()
				if _time < time then
					_time = _time + 1
					self.label_cd.text = _time .. " s"
				end
			end,1,-1)

		end
	end
	AddClick(self.btn,click_func)
end

function TimerView:ClearTimer()
	TimerQuest:GetInstance():ClearTimer(self.timer_1)
	TimerQuest:GetInstance():ClearTimer(self.timer_2)
end

function TimerView:destroy_callback()
	self:ClearTimer()
	if self.item then
		self.item:DeleteMe()
	end
end

function TimerView:__delete()
end