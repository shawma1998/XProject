Camera = BaseClass:Extend()

local Input = CSE.Input

function Camera:New(o)
    local o = Camera.super.New(self)
    o:Init()
    return o
end

function Camera:GetInstance()
    if self.instance == nil then
        self.instance = self:New()
    end
    return self.instance
end

function Camera:Init(  )
    self.curr_time = 0
    self.cameraObj = GameObject.Find("Main Camera")
    self.cameraTrans = self.cameraObj.transform
    --事件

    self.tarPos = self.cameraTrans.localPosition
    self.tar_distance = self.cameraTrans.localPosition.y

    self.on_camera_move_id = GlobeEvent:Bind(Events.CAMERA_MOVE,function(x,z)
        self:OnCameraMove(-x, nil ,-z)
    end)

    self.on_camera_zoom_id = GlobeEvent:Bind(Events.CAMERA_ZOOM,function(y)
        self:OnCameraZoom(-y)
    end)
    UpdateBeatManager:GetInstance():AddUpdateBeat(self)
end

function Camera:OnCameraMove(x , y ,z)
    if x then
        x = x / 3
    end
    if z then
        z = z / 3
    end
    local curr_x = self.cameraTrans.localPosition.x
    local curr_y = self.cameraTrans.localPosition.y
    local curr_z = self.cameraTrans.localPosition.z
    self.is_update = true
    self.tarX = x and (curr_x + x) or curr_x
    self.tarZ = z and (curr_z + z) or curr_z
    self.tarY = y and (curr_y + y) or curr_y
    self.tarPos = Vector3(self.tarX,self.tarY,self.tarZ)
    ----self.cameraTrans.localPosition = Vector3(self.tarX,self.tarY,self.tarZ)
    self.curr_time = 0
end

function Camera:OnCameraZoom(y)
    self.tar_distance = self.cameraTrans.localPosition.y + (y * 30)
end

function Camera:Update( time,deltaTime )
    local position = Vector3.Lerp(self.cameraTrans.localPosition, self.tarPos, deltaTime)
    self.distance = CSE.Mathf.Lerp(self.cameraTrans.localPosition.y, self.tar_distance, deltaTime * 10)
    self.cameraTrans.localPosition = Vector3(position.x,self.distance,position.z)
end
