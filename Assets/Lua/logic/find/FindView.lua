FindView = BaseView:Extend()

function FindView:New()
	local o = BaseView.super.New(self)
	o.abName = "ui_find"
	o.resName = "FindView"
	o.layer = ENUM.UI_LAYER.TOP
	o.hide_pre_view = true
	return o
end

function FindView:loadSuccess()
	self.node_parent = GetNode(self.transform,{"node_parent"})
	self.item_list = {}
	--创建8 X 8
	for i = 1, 8 do
		for j = 1, 8 do
			self.item_list[i] = self.item_list[i] or {}
			self.item_list[i][j] = FindItem:New(self.node_parent)
			self.item_list[i][j]:SetPosition(i*110,-j*110)
		end
	end

	local vo = FindVO:New()
	vo:SetData({ x = 2 , y = 3 } , {x = 6 , y = 6},function(result)
		for i, v in pairs(result) do
			print(v.x,v.y)
			self.item_list[v.x][v.y]:SetSelect(true)
		end
	end)
	self:InitEvent()
end

function FindView:InitEvent()
end

function FindView:destroy_callback()
end

function FindView:__delete()
	for i, v in pairs(self.item_list) do
		for i2, v2 in pairs(v) do
			v2:DeleteMe()
		end
	end
	self.item_list = nil
end