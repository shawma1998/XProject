FindItem = BaseItem:Extend()

function FindItem:New(...)
	local o = FindItem.super.New(self,...)
	o.abName = "ui_find"
	o.resName = "FindItem"
	o:Load()
	--self.hide_pre_view = true
	return o
end

function FindItem:loadSuccess()
	self.img_res = GetNode(self.transform,{"img_res"},"Image")
	self:InitEvent()
	if self.needRefresh then
		self:SetData(self.data)
	end
	if self.needSelect then
		self:SetSelect(self.select)
	end
end

function FindItem:SetData(data)
	self.data = data
	if self.loaded then
		self.needRefresh = false

	else
		self.needRefresh = true
	end
end


function FindItem:InitEvent ()

end

function FindItem:SetSelect(select)
	self.select = select
	if self.loaded then
		self.needSelect = false
		self.img_res.color = CS.UnityEngine.Color(1,0,0,1)
	else
		self.needSelect = true
	end
end

function FindItem:__delete()

end
