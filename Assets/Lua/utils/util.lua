
function GetUILayer(layer)
	return GameObject.Find("root/Canvas/"..layer)
end

--获取节点
function GetNode(trans , node_list , s_type)
    s_type = s_type or "Transform"
    if string.find(s_type , "_") then
        local temp = {}
        for i, v in ipairs(node_list) do
            s_type = string.gsub(s_type,"_","")
            table.insert(temp,trans:Find(v):GetComponent(s_type))
            return table.unpack(temp)
        end
    else
        local list = CS.UINodeHelper.GetUINodes(trans,node_list,s_type or "Transform")
        local temp = {}
        for i = 0, list.Length - 1, 1 do
            table.insert(temp,list[i])
        end
        return table.unpack(temp)
    end
end

function AddClick(obj,func)
	if obj then
		CS.ClickListener.AddClickHandler(obj,function (_obj,x,y)
			if func then
				func(_obj,x,y)
			end
		end)
	end
end

--打印某个table
function PrintTable(table , level)
  local key = ""
  level = level or 1
  local indent = ""
  for i = 1, level do
    indent = indent.."  "
  end
  if key ~= "" then
    print(indent..key.." ".."=".." ".."{")
  else
    print(indent .. "{")
  end
  key = ""
  for k,v in pairs(table) do
     if type(v) == "table" then
        key = k
        PrintTable(v, level + 1)
     else
        local content = string.format("%s%s = %s", indent .. "  ",tostring(k), tostring(v))
      print(content)  
      end
  end
  print(indent .. "}")
end

function ToStringEx(value,is_key)
    if type(value)=='table' then
        return TableToStr(value)
    elseif type(value)=='string' then
        if not is_key then
            return "\'"..value.."\'"
        end
        --return "\'"..value.."\'"
        return value
    else
        return tostring(value)
    end
end

function TableToStr(t)
    if t == nil then return "" end
    local retstr= "{"

    local i = 1
    for key,value in pairs(t) do
        local signal = ","
        if i==1 then
            signal = ""
        end

        if key == i then
            retstr = retstr..signal..ToStringEx(value)
        else
            if type(key)=='number' or type(key) == 'string' then
                retstr = retstr..signal..''..ToStringEx(key,true).."="..ToStringEx(value)
            else
                if type(key)=='userdata' then
                    retstr = retstr..signal.."*s"..TableToStr(getmetatable(key)).."*e".."="..ToStringEx(value)
                else
                    retstr = retstr..signal..key.."="..ToStringEx(value)
                end
            end
        end

        i = i+1
    end

    retstr = retstr.."}"
    return retstr
end

function StrToTable(str)
    if str == nil or type(str) ~= "string" or str == "" then
        return {}
    end

    return load("return " .. str)()
end

function GetTime()
    return os.time()
end

--将 szFullString 对象拆分为一个子字符串表
function Split(szFullString, szSeparator, start_pos)
    if not szFullString or not szSeparator then
        return {}
    end
    local nFindStartIndex = start_pos or 1
    local nSplitIndex = 1
    local nSplitArray = {}
    while true do
        local nFindLastIndex = string.find(szFullString, szSeparator, nFindStartIndex)
        if not nFindLastIndex then
            nSplitArray[nSplitIndex] = string.sub(szFullString, nFindStartIndex, string.len(szFullString))
            break
        end
        table.insert(nSplitArray, string.sub(szFullString, nFindStartIndex, nFindLastIndex - 1))
        nFindStartIndex = nFindLastIndex + string.len(szSeparator)
        nSplitIndex = nSplitIndex + 1
    end
    return nSplitArray
end

function FormatTab( tab )
    for i, v in pairs(tab) do
        local num = tonumber(i)
        if num and not tab[num] then
            tab[num] = v
        end
    end
    return tab
end

--发送网络协议
function Send(id,data)
    data = data or {}
    NetManager:GetInstance():Send(id , data)
end

--创建item列表
function GenItemList (that , str , content , class , data , set_func , temp)
    data = data or {}
    if not that.item_list then
        that.item_list = {}
    end
    if not that.temp_list then
        that.temp_list = {}
    end
    local item_list = that.item_list[str .. "_items"] or {}
    local temp_obj = that.temp_list[str .. "_temp_obj"]
    if not temp_obj then
        temp_obj = temp or content.transform:GetChild(0).gameObject
        that.temp_list[str .. "_temp_obj"] = temp_obj
    end
    temp_obj:SetActive(true)
    for i, v in ipairs(data) do
        item_list[i] = item_list[i] or class:New(nil,GameObject.Instantiate(temp_obj,content.transform))
        item_list[i]:SetVisible(true)--?
        if set_func then
            set_func(item_list[i],i,v)
        end
    end
    for i = #data + 1, #item_list do
        item_list[i]:SetVisible(false)
    end
    that.item_list[str .. "_items"] = item_list
    temp_obj:SetActive(false)
end


