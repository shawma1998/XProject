ENUM = {}


ENUM.UI_LAYER = {
	UI = "UI",
	MAIN = "Main",
	TOP = "Top",
}

ENUM.Depth = {
	["Main"] = 100,
	["UI"] = 300,
	["Top"] = 600,
}