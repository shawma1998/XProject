TweenMotion = TweenMotion or {}

function TweenMotion.liner(rate)
    return rate
end

function TweenMotion.pingPong(ratio)
    if ratio < 0.5 then
        return TweenMotion.liner(ratio * 2)
    else
        return TweenMotion.liner( 2 - ratio * 2 )
    end
end