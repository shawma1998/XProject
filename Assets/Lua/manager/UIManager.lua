UIManager = BaseClass:Extend()

local view_id = -1

function UIManager:New()
	local o = UIManager.super.New(self)
	o.curr_view_queue = {}
	return o
end

function UIManager:GetInstance()
	if self.instance == nil then
		self.instance = self:New()
	end
	return self.instance
end

function UIManager:AddUIDepth(obj,layer)
	ENUM.Depth[layer] = ENUM.Depth[layer] + 1
	CS.UIDepth.SetDepth(obj,ENUM.Depth[layer])
end

function UIManager:ReduceUIDepth(obj,layer)
	ENUM.Depth[layer] = ENUM.Depth[layer] - 1
	CS.UIDepth.SetDepth(obj,ENUM.Depth[layer])
end

--隐藏前面的界面
function UIManager:HidePreView (that)
	for i, v in pairs(self.curr_view_queue) do
		v:SetVisible(v.view_id == that.view_id)
	end
end

--加载某个界面
function UIManager:LoadView(view_class)
	local class = _G[view_class] or LuaMemManager.Load(view_class)
	view_id = view_id + 1
	local view
	if not self.curr_view_queue[view_class] then
		view = class:New()
		view.view_id = view_id
		self.curr_view_queue[view_class] = view
		view:Open()
	else
		view = self.curr_view_queue[view_class]
		view.view_id = view_id
		--走置顶界面流程
		view:Open()
	end
	return view
end

--关闭某个界面
function UIManager:HideView(view_class)
	local view = self.curr_view_queue[view_class]
	if view then
		view = self.curr_view_queue[view_class]
		view.view_id = view_id
		--走置顶界面流程
		view:Close()
	end
end

--移除某个组件
function UIManager:DeleteItem(that)
	GameObject.DestroyImmediate(that.gameObject,true)
end

--移除某个界面
function UIManager:DeleteView(that)
	if that.is_deleting then return end
	that.is_deleting = true
	GameObject.DestroyImmediate(that.gameObject,true)
	if that.layer then
		--对应层级减1
		ENUM.Depth[that.layer] = ENUM.Depth[that.layer] - 1
	end
	--队列中删除界面
	if self.curr_view_queue[that.resName] then
		self.curr_view_queue[that.resName] = nil
	end

	if that.hide_pre_view then
		--显示view_id最大的界面
		local max_id = false
		for i, v in pairs(self.curr_view_queue) do
			if (not max_id or (max_id and max_id < v.view_id)) and (that.layer == v.layer or v.layer == ENUM.UI_LAYER.MAIN) then
				max_id = v.view_id
			end
		end
		if max_id then
			for i, v in pairs(self.curr_view_queue) do
				if v.view_id == max_id then
					v:SetVisible(true)
				end
			end
		end
	end
end
