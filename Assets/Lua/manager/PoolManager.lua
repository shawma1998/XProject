--对象池管理器
PoolManager = BaseClass:Extend()

--对象池最大值
PoolManager.MAX = {
    default = 10,
}

local insert = table.insert

function PoolManager:New()
    local o = PoolManager.super.New(self)
    o:Init()
    o.pool_cache_list = {}   --[itemClass] = {objs...}
    return o
end

function PoolManager:GetInstance()
    if self.instance == nil then
        self.instance = self:New()
    end
    return self.instance
end

function PoolManager:Init(  )
    self.list_cache_layer = {}
    self.pool_root = self.pool_root or self:CreatePoolRoot()
    print(self.pool_root)
end

function PoolManager:CreatePoolRoot ()
    local root = false
    root = GameObject("pool_root").transform
    root.parent = GameObject.Find("root").transform
    return root.transform
end

--从对象池中拿
function PoolManager:GetObject (item_type,parent)
    --print("----GetObject----")
    if type(item_type) ~= "string" then return end
    self.pool_cache_list[item_type] = self.pool_cache_list[item_type] or {}
    local item = false
    if #self.pool_cache_list[item_type] <= 0 then
        --创建数量
        item = _G[item_type]:New(parent)
    else
        item = table.remove(self.pool_cache_list[item_type],1)
        item.transform:SetParent(parent)
    end
    return item
end

--从对象池中放回
function PoolManager:SetObject (item_type,item)
    if type(item_type) ~= "string" then return end
    self.pool_cache_list[item_type] = self.pool_cache_list[item_type] or {}
    if #self.pool_cache_list[item_type] >= self:GetMaxCount(item_type) then
        --超过上限就删了
        if item.Release then
            item:Release()
        end
        item:DeleteMe()
    else
        --print("----SetObject----")
        insert(self.pool_cache_list[item_type],item)
        if item.Release then
            item:Release()
        end
        item.transform:SetParent(self.pool_root)
    end
end

function PoolManager:GetMaxCount (item_type)
    return PoolManager.MAX[item_type] or PoolManager.MAX.default
end
