TimerManager = BaseClass:Extend()
local insert = table.insert
function TimerManager:New(o)
    local o = TimerManager.super.New(self)
    o:Init()
    return o
end

function TimerManager:Init(  )
    self.curr_time = 0
    self.timer_id = -1
    self.dic_timer_list = {}
    UpdateBeatManager:GetInstance():AddUpdateBeat(self)
    --GlobeUpdateManager:AddUpdateBeat(self)
end

function TimerManager:GetInstance()
    if self.instance == nil then
        self.instance = self:New()
    end
    return self.instance
end

function TimerManager:AddDelay(func , time , ...)
    self.timer_id = self.timer_id + 1
    self.dic_timer_list[self.timer_id] = {
        delay = time,
        func = func,
        start = self.curr_time,
        args = ...,
        id = self.timer_id,
        type = "delay"
    }
    return self.timer_id
end

function TimerManager:AddRepeat(func , delay_time , repeat_times , ...)
    self.timer_id = self.timer_id + 1
    self.dic_timer_list[self.timer_id] = {
        delay = delay_time,
        func = func,
        start = self.curr_time,
        args = ...,
        id = self.timer_id,
        type = "repeat",
        times = repeat_times,
        curr_times = 0
    }
    return self.timer_id
end

function TimerManager:Update (time,deltaTime)
    self.curr_time = time
    for i, v in pairs(self.dic_timer_list) do
        self:HandlerTimer(v)
    end
end

function TimerManager:HandlerTimer (vo)
    if vo.type == "delay" and self.curr_time >= vo.start + vo.delay then
        if vo.func then
            vo.func(vo.args)
            self:ClearTimer(vo.id)
        end
    end
    if vo.type == "repeat" and (not vo.delay or self.curr_time >= vo.start + vo.delay) then
        if vo.func then
            vo.start = self.curr_time
            vo.curr_times = vo.curr_times + 1
            vo.func(vo.args)
            if vo.times ~= -1 and vo.curr_times >= vo.times then
                self:ClearTimer(vo.id)
            end
        end
    end
end

function TimerManager:ClearTimer (id)
    local del_vo = self.dic_timer_list[id]
    if del_vo then
        self.dic_timer_list[id] = nil
    end
end
