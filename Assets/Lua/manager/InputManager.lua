InputManager = BaseClass:Extend()

local Input = CSE.Input

function InputManager:New(o)
    local o = InputManager.super.New(self)
    o:Init()
    return o
end

function InputManager:GetInstance()
    if self.instance == nil then
        self.instance = self:New()
    end
    return self.instance
end

function InputManager:Init(  )
    self.curr_Pos = {}
    UpdateBeatManager:GetInstance():AddUpdateBeat(self)
end

function InputManager:Update(  )
    if Input.GetMouseButtonDown(0) then
        --self.start_x = Input.mousePosition.x  self.start_y = Input.mousePosition.y

    end

    --if Input.GetMouseButton(0) then
    --    --左键
    --    self:HandlerClick(Input.mousePosition.x - self.start_x, Input.mousePosition.y - self.start_y)
    --    self.start_x = Input.mousePosition.x  self.start_y = Input.mousePosition.y
    --end
    --if Input.GetMouseButtonUp(0) then
    --    --左键抬起时
    --    self:HandlerClick(Input.mousePosition.x - self.start_x, Input.mousePosition.y - self.start_y)
    --end
    ----滚轮
    --self.distance = Input.GetAxis("Mouse ScrollWheel")
    --if self.distance ~= 0 then
    --    self:HandlerZoom(self.distance)
    --end
    --
    --if Input.touchCount == 1 then
    --    self.curr_Pos = Input.touches[0].position   --记录手指刚触碰的位置
    --    self:HandlerClick(self.curr_Pos.x, self.curr_Pos.y)
    --end
end

function InputManager:HandlerClick(x,z)
    GlobeEvent:Fire(Events.CAMERA_MOVE,x,z)
end

function InputManager:HandlerZoom(y)
    GlobeEvent:Fire(Events.CAMERA_ZOOM,y)
end