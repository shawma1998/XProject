UpdateBeatManager = BaseClass:Extend()

function UpdateBeatManager:New()
    local o = UpdateBeatManager.super.New(self)
    self:Init()
    return o
end

function UpdateBeatManager:GetInstance()
    if self.instance == nil then
        self.instance = self:New()
    end
    return self.instance
end

function UpdateBeatManager:Init(  )
    self.update_run_list = {}
    self.curr_update_frame = 0

    CS.UpdateBeatManager.Instance:AddUpdateBeat(self.__id,function(time , deltaTime)
        self:Update(time,deltaTime)
    end)
end


function UpdateBeatManager:AddUpdateBeat (that,frame)
    if not that["Update"] or type(that["Update"]) ~= "function" then
        print("添加update方法错误")
        return
    end
    self.update_run_list[that.__id] = self.update_run_list[that.__id] or {func = that["Update"] , frame = frame or 1 , curr_frame = 1 , that = that}
end

--移除Update
function UpdateBeatManager:RemoveUpdate (that)
    if self.update_run_list[that.__id] then
        self.update_run_list[that.__id] = nil
    end
end

function UpdateBeatManager:Update (time,deltaTime)
    for i, v in pairs(self.update_run_list) do
        if v.curr_frame >= v.frame then
            if v.func then
                v.func(v.that,time,deltaTime)
            end
        else
            v.curr_frame = v.curr_frame + 1
        end
    end
end

