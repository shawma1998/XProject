EventManager = BaseClass:Extend()
local insert = table.insert

function EventManager:New()
    self.dic_event = {}
    self.event_id = 0
    return EventManager.super.New(self)
end

function EventManager:Bind(event_name,func)
    self.dic_event[event_name] = self.dic_event[event_name] or {}
    self.event_id = self.event_id + 1
    self.dic_event[event_name][self.event_id] = {
        func = func,
        id = self.event_id
    }
    return self.event_id
end

function EventManager:UnBind(event_id)
    local quit = false
    local name = false
    for i, v in pairs(self.dic_event) do
        if quit then break end
        for i2, v2 in pairs(v) do
            if v2.id == event_id then
                name = i
                quit = true
                break
            end
        end
    end
    if name and self.dic_event[name] and self.dic_event[name][event_id] then
        self.dic_event[name][event_id] = nil
    end
end

function EventManager:Fire(event_name , ...)
    for i, v in pairs(self.dic_event) do
        if i == event_name then
            for i2, v2 in pairs(v) do
                if v2.func then
                    v2.func(...)
                end
            end
            break
        end
    end
end


