PanelManager = BaseClass:Extend()


function PanelManager:New(o)
	self:Init()
	return PanelManager.super.New(self)
end

function PanelManager:Init(  )
	self.list_cache_layer = {}
end
 
function PanelManager:GetInstance()
	if self.instance == nil then
		self.instance = self:New()
	end
	return self.instance
end
 
function PanelManager:GetPannelLayerParent(layer)
	self.list_cache_layer[layer] = self.list_cache_layer[layer] or GetUILayer(layer)
	return self.list_cache_layer[layer]
end

function PanelManager:CreateUI(abName , abRes , func)
end