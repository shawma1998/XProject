CookieManager = BaseClass:Extend()

local CS_CookieMgr= CS.CookieManager.Instance

function CookieManager:New(o)
    self:Init()
    return CookieManager.super.New(self)
end

function CookieManager:GetInstance()
    if self.instance == nil then
        self.instance = self:New()
    end
    return self.instance
end

function CookieManager:Init(  )
end

function CookieManager:GetCookie(key)
    local content = CS_CookieMgr:GetCookieContent()
    content = content == "" and {} or content
    local cookie_tab = StrToTable(content)
    return cookie_tab[key]
end

function CookieManager:SetCookie(key,val)
    local content = CS_CookieMgr:GetCookieContent()
    local cookie_tab = StrToTable(content)
    cookie_tab[key] = val
    content = TableToStr(cookie_tab)
    CS_CookieMgr:SetCookieContent(content)
end