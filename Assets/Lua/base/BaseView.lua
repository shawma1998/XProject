BaseView = BaseClass:Extend()

local insert = table.insert

function BaseView:New()
	local o = BaseView.super.New(self)
	o:Init()
	return o
end

function BaseView:Init()
	self.abName = self.abName
	self.resName = self.resName
	self.layer = self.layer or ENUM.UI_LAYER.UI
	self.loadSuccess = self.loadSuccess or nil
	self.event_list = nil
	self.loaded = false
end

function BaseView:Open ()
	local loadUICallback = function(obj,skip_load_succ)
		self.is_deleting = false
		self.loaded = true
		self.gameObject = obj
		self.transform = obj.transform
		self.transform.sizeDelta = Vector2(CanvasWidth,CanvasHeight)
		if self.loadSuccess and not skip_load_succ then
			self:loadSuccess()
		end
		--层级
		UIManager:GetInstance():AddUIDepth(self.gameObject,self.layer)
		self.transform:SetAsLastSibling()
		if self.hide_pre_view then
			--是否隐藏上一个界面?
			UIManager:GetInstance():HidePreView(self)
		end
		if self.need_refreshVisible then
			self:SetVisible(self.state)
		end

		local close_btn = GetNode(self.transform,{"btn_close"},"GameObject")
		if close_btn then
			AddClick(close_btn,function ()
				self:SetClose()
			end)
		end

		if self.OnShow then
			self:OnShow()
		end
	end
	if not self.loaded then
		ResManager:GetInstance():CreateView (self,self.abName,self.resName,self.layer,loadUICallback)
	else
		loadUICallback(self.gameObject,false)
	end
end

function BaseView:SetVisible (state)
	self.state = state
    if self.loaded then
	    self.need_refreshVisible = false
		self.gameObject:SetActive(self.state)
    else
    	self.need_refreshVisible = true
    end
end

function BaseView:BindEvent (dispatcher,event_id,func)
	local id = dispatcher:Bind(event_id,func)
	self.event_list = self.event_list or {}
	self.event_list[id] = {dispatcher = dispatcher}
end

function BaseView:SetClose()
	if self.OnClose then
		self:OnClose()
	end
	self:Close()
end

function BaseView:Close ()
	self:DeleteMe()
end

function BaseView:__delete ()
	self:ClearItemList()
	if self.destroy_callback and self.loaded then
		self:destroy_callback()
	end
	
	--解绑事件
	if self.event_list and next(self.event_list) then
		for i, v in pairs(self.event_list) do
			v.dispatcher:UnBind(i)
		end
	end
	self.event_list = nil
	self.loaded = false
	UIManager:GetInstance():DeleteView(self)
	--清除资源引用
	ResManager:GetInstance():ClearAllRef(self)
end

function BaseView:ClearItemList()
	if self.item_list then
		for i, v in pairs(self.item_list) do
			for i2, v2 in ipairs(v) do
				v2:DeleteMe()
			end
		end
		self.item_list = false
	end

	self.temp_list = false
end