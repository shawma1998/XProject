BaseItem = BaseClass:Extend()

local insert = table.insert

function BaseItem:New(parent,prefab)
    local o = BaseItem.super.New(self)
    o.prefab = prefab
    o.parent = parent
    o:Init()
    return o
end

function BaseItem:Init()
    self.abName = self.abName
    self.resName = self.resName
    self.loadSuccess = self.loadSuccess or nil
    self.event_list = nil
    self.loaded = false
end

function BaseItem:Load ()
    local loadUICallback = function(obj)
        self.loaded = true
        self.gameObject = obj
        self.transform = obj.transform
        if self.parent then
            self.transform:SetParent(self.parent)
            self.transform.localPosition = Vector3(0,0,0)
        end
        if self.loadSuccess then
            self:loadSuccess()
        end
        if self.need_refreshVisible then
            self:SetVisible(self.state)
        end
        if self.need_refreshPos then
            self:SetPosition(self.x,self.y)
        end
    end

    if self.prefab then
        loadUICallback(self.prefab.gameObject)
    else
        if not self.loaded then
            ResManager:GetInstance():CreateItem(self,self.abName,self.resName,loadUICallback)
        else
            loadUICallback(self.gameObject)
        end
    end
end

function BaseItem:SetVisible (state)
    self.state = state
    if self.loaded then
        self.need_refreshVisible = false
        self.gameObject:SetActive(self.state)
    else
        self.need_refreshVisible = true
    end
end

function BaseItem:BindEvent (dispatcher,event_id,func)
    local id = dispatcher:Bind(event_id,func)
    self.event_list = self.event_list or {}
    self.event_list[id] = {dispatcher = dispatcher}
end

function BaseItem:__delete ()
    self:ClearItemList()
    if self.destroy_callback and self.loaded then
        self:destroy_callback()
    end
    --解绑时间
    if self.event_list and next(self.event_list) then
        for i, v in pairs(self.event_list) do
            v.dispatcher:UnBind(i)
        end
    end
    self.event_list = nil

    self.loaded = false
    --销毁item
    UIManager:GetInstance():DeleteItem(self)
    --清除资源引用
    ResManager:GetInstance():ClearAllRef(self)
    self.prefab = false
end


function BaseItem:SetPosition (x,y)
    self.x,self.y = x,y
    if self.loaded then
	    self.need_refreshPos = false
        self.transform.localPosition = Vector3(self.x,self.y)
    else
    	self.need_refreshPos = true
    end
end

function BaseItem:ClearItemList()
    if self.item_list then
        for i, v in pairs(self.item_list) do
            for i2, v2 in ipairs(v) do
                v2:DeleteMe()
            end
        end
        self.item_list = false
    end

    self.temp_list = false
end