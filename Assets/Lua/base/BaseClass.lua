BaseClass = {}
BaseClass.__index = BaseClass
__ins_id = 0		--实例化的id
function BaseClass:New(...)
	local o = {...}
	self.__index = self

	local function localDeleteFunc(self)
		--从子类开始delete
		local now_super = self
		while now_super ~= nil do
			if now_super.__delete then
				now_super.__delete(self)
			end
			now_super = now_super.super
		end
	end
	o.DeleteMe = localDeleteFunc

	o.__id = __ins_id
	__ins_id = __ins_id + 1
	return setmetatable(o,self)
end

function BaseClass:Extend()
	local sub = {}
	sub.__index = sub
	sub.super = self
	setmetatable(sub,self)
	return sub
end