Curr_Platform = CS.UnityEngine.Application.platform:ToString()
CSE = CS.UnityEngine
NewObj = CS.Utils.NewObj
Vector3 = CSE.Vector3
Vector2 = CSE.Vector2
GameObject = CS.UnityEngine.GameObject
ScreenWidth = CS.UnityEngine.Screen.width
ScreenHeight = CS.UnityEngine.Screen.height
IsNull = CS.Utils.IsNull
CanvasWidth = GameObject.Find("root/Canvas").transform.sizeDelta.x
CanvasHeight = GameObject.Find("root/Canvas").transform.sizeDelta.y


if CS.Const.LOCAL and Curr_Platform == "WindowsEditor" then
	package.cpath = package.cpath .. ';C:/Users/Shaw/AppData/Roaming/JetBrains/IdeaIC2022.1/plugins/EmmyLua/debugger/emmy/windows/x64/?.dll'
	local dbg = require('emmy_core')
	dbg.tcpListen('localhost', 9966)
	package.path = package.path..";".. CS.UnityEngine.Application.dataPath.."/Lua/?.lua"
elseif CS.Const.GAME_PACKAGE == CS.GAME_PLATFORM.WEBGL_WECHAT_MINIGAME then
	--是WeChat -- mini game---
	package.path = package.path..";".. CS.Utils.GetLuaPackageRoot() .."/AssetBundle/lua/?.lua"
else
	package.path = package.path..";".. CS.UnityEngine.Application.persistentDataPath.."/AssetBundle/lua/?.lua"
end

--引入cjson
--package.path = package.path..";".. CS.UnityEngine.Application.dataPath.."/extend/cjson.dll"


--require = function ( path , name )
--	_G[name] = old_require(path)
--end

require("utils.util")
json = require("utils.json")
require("manager.baseRequire")
--重写原本lua的requeire
require("manager.LuaMemManager")
require("manager.ResManager")
require("manager.NetManager")
LuaMemManager.ReSetRequire()
require("Config")

GlobeEvent = EventManager:New()
TimerQuest = TimerManager:GetInstance()
PoolManager:GetInstance()
InputManager:GetInstance()
ResManager:GetInstance()
NetManager:GetInstance()
require("Requirement")

UpdateBeatManager:GetInstance():AddUpdateBeat(LuaMemManager)
--GlobeUpdateManager = UpdateBeatManager:New()

--游戏主逻辑
local function GameStart( ... )
	--[[管理器载入]]
	print("-------------GAME_START-------------------")
	Camera:GetInstance()
	UIManager:GetInstance():LoadView("AccountView")
end
GameStart()






