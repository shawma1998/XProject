Events = {}
Events.TEST_EVENT = "Events.TEST_EVENT"
Events.CAMERA_MOVE = "Events.CAMERA_MOVE"
Events.CAMERA_ZOOM = "Events.CAMERA_ZOOM"

--服务器连接成功
Events.CONNECT_SUCCESS = "Events.CONNECT_SUCCESS"


--玩家数据更新
Events.PLAYER_INFO_CHANGED = "Events.PLAYER_INFO_CHANGED"


--大厅房间列表刷新
Events.CARD_ROOM_LIST_REFRESH = "Events.CARD_ROOM_LIST_REFRESH"
--房间等待数据刷新
Events.UPDATE_READY_ROOM_DATA = "Events.UPDATE_READY_ROOM_DATA"
Events.UPDATE_CARDS = "Events.UPDATE_CARDS"