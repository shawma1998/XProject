require("common.message.MessageItem")
require("common.message.MessageView")
Message = Message or {}


Message.MAX_NUM = 3

function Message:Init()
    self.view = false
end

function Message.Show(msg)
    if not self.view then
        self.view = UIManager:LoadView("MessageView")
    end
    self.view:ShowMessageSimple(msg)
end


Message:Init()
return Message