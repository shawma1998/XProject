MessageItem = BaseItem:Extend()

function MessageItem:New(...)
    local o = MessageItem.super.New(self,...)
    o.abName = "ui_common"
    o.resName = "MessageItem"
    o:Load()
    --self.hide_pre_view = true
    return o
end

function MessageItem:loadSuccess()
    self.label_name = GetNode(self.transform,{"label_name"},"Text")
    --self.btn_join = GetNode(self.transform,{"btn_join"},"GameObject")

    self:InitEvent()
    if self.needRefresh then
        self:SetData(self.msg)
    end
end

function MessageItem:SetData(msg)
    self.msg = msg
    if self.loaded then
        self.needRefresh = false
        self.label_name.text = self.msg
        self:PlayTween()
    else
        self.needRefresh = true
    end
end

function MessageItem:PlayTween()
    if self.t_id then
        TweenManager:GetInstance():RemoveTween(self.t_id)
    end

    local info = {
        ani_parent = self.node_pos,
        t_type = TweenType.POS,
        start = false,
        target = Vector2(300,204),
        time = 2,
        motion_type = TweenMotion.liner,
        loop = true,
        call_back = function()
            self:PlayTweenEnd()
        end
    }
    self.t_id = TweenManager:GetInstance():AddTween (self, info)
end

function MessageItem:PlayTweenEnd()
    if self.that then
        self.that:PlayEndHandler(self)
    end
end

function MessageItem:SetMessageView(that)
    self.that = that
end

function MessageItem:InitEvent ()
end

function MessageItem:__delete()
    TweenManager:GetInstance():RemoveTween(self.t_id)
    self.t_id = false
end
