﻿using UnityEngine;
using UnityEditor;
using System.IO;

public class AssetImportHandler : AssetPostprocessor
{
    public void OnPreprocessTexture()
    {
        if (assetImporter.assetPath.Contains("/UI/") && assetImporter.assetPath.Contains("/texture/"))
        {
            TextureImporter texImpoter = assetImporter as TextureImporter;
            texImpoter.textureType = TextureImporterType.Sprite;
            //texImpoter.assetBundleName
            var _path = assetImporter.assetPath.Replace("Assets/", "");
            var args = _path.Split('/');
            var name = args[0].ToLower() + "_" + args[1].ToLower();
            assetImporter.assetBundleName = name+"_texture";
            texImpoter.spritePackingTag = name;
            assetImporter.assetBundleVariant = "u3d";
            EditorUtility.SetDirty(texImpoter);
        }
        //icon大图 单独打包
        if (assetImporter.assetPath.Contains("/Icon/"))
        {
            var path = assetImporter.assetPath.Replace("Assets/", "");
            path = path.Replace(".png", "");
            path = path.Replace(".jpg", "");
            path = path.Replace("/", "_");
            TextureImporter texImpoter = assetImporter as TextureImporter;
            texImpoter.textureType = TextureImporterType.Sprite;
            //texImpoter.assetBundleName
            assetImporter.assetBundleName = path;
            assetImporter.assetBundleVariant = "u3d";
            EditorUtility.SetDirty(texImpoter);
        }
    }
    public static void SetABTagToPrefab()
    {
        string assetPath = "Assets/UI";
        string[] uids = AssetDatabase.FindAssets("t:Prefab t:Texture", new string[] { assetPath });

        if (uids == null)
        {
            return;
        }

        string filePath = "";
        AssetImporter assetImporter = null;


        for (int i = 0; i < uids.Length; i++)
        {
            var uid = uids[i];
            filePath = AssetDatabase.GUIDToAssetPath(uid);
            assetImporter = AssetImporter.GetAtPath(filePath);
            if (assetImporter.assetPath.Contains("UI/") && assetImporter.assetPath.Contains("prefab/"))
            {
                var _path = assetImporter.assetPath.Replace("Assets/", "");
                var args = _path.Split('/');
                var name = args[0].ToLower() + "_" + args[1].ToLower();
                assetImporter.assetBundleName = name + "_prefab";
                assetImporter.assetBundleVariant = "u3d";
                EditorUtility.SetDirty(assetImporter);
            }
        }
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
    }
    public static void SetABTagToFont()
    {
        string assetPath = Application.dataPath + "/Font";
        
        FileInfo[] files = new DirectoryInfo(assetPath).GetFiles();

        foreach (var file in files)
        {
            var direName = file.Directory.Name;
            if(file.Extension != ".meta")
            {
                Debug.Log(Utils.GetRelativePath(file.FullName));
                AssetImporter import = AssetImporter.GetAtPath(Utils.GetRelativePath(file.FullName));
                import.assetBundleName = direName + "_asset";
                import.assetBundleVariant = "u3d";
                EditorUtility.SetDirty(import);
            }
        }

        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
    }
}
