using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ServerSelectorView : MonoBehaviour
{
    private Action actionCallback;
    private Button btn_Reload;
    private Dropdown dropdown;
    private InputField inputField;
    private InputField portField;
    private string ip;
    private string port;
    private List<string> opitions = new List<string> { "本地@127.0.0.1:8885", "本地2@192.168.31.81:80", "远程@43.138.141.127:8885" };
    private void Awake()
    {
        btn_Reload = this.transform.Find("Button").GetComponent<Button>();
        inputField = this.transform.Find("InputField").GetComponent<InputField>();
        portField = this.transform.Find("port").GetComponent<InputField>();
        dropdown = this.transform.Find("Dropdown").GetComponent<Dropdown>();

        dropdown.AddOptions(opitions);
    }
    // Start is called before the first frame update
    void Start()
    {
        btn_Reload.onClick.AddListener(() => {
            Const.RES_IP = $"http://{ip}:{port}/";
            this.actionCallback?.Invoke();
        });
        dropdown.onValueChanged.AddListener((val) => {
            var ipstr = opitions[val];
            ipstr = ipstr.Split('@')[1];
            ipstr = ipstr.Replace('：', ':');
            ip = ipstr.Split(':')[0];
            port = ipstr.Split(':')[1];
            inputField.text = ip;
            portField.text = port;
        });
        inputField.onValueChanged.AddListener((ip) => {
            this.ip = ip;
        });
        portField.onValueChanged.AddListener((port) => {
            this.port = port;
        });
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void AddSelectCallBack(Action p)
    {
        this.actionCallback = p;
    }
}
