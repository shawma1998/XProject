﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GAME_PLATFORM {
    DEFAULT,
    WEBGL_WECHAT_MINIGAME,
}

public class Const : MonoBehaviour
{
    //资源加载的IP地址
    public static string RES_IP = @"http://192.168.31.81/";
    public const string RES_PATH = @"AssetBundle/";
    public const string VERSION_FILE_NAME = @"version.xml";
    public static bool LOCAL = true;
    public GAME_PLATFORM _GAME_PACKAGE = GAME_PLATFORM.DEFAULT;  
    public static GAME_PLATFORM GAME_PACKAGE = GAME_PLATFORM.DEFAULT;  
    public static RuntimePlatform PLATFORM = RuntimePlatform.Android;  
    public const string prefix = ".u3d";


    private void Awake(){

        Const.PLATFORM = Application.platform;
        Const.GAME_PACKAGE = this._GAME_PACKAGE;
    }

    private void Start()
    {
        
    }
}
