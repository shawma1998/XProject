using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using XLua;

public class LuaManger : Singleton<LuaManger>
{
    public LuaEnv luaenv;
    // Start is called before the first frame update
    void Awake()
    {
        string luaPath = "";
#if WECHAT_MINI_GAME
            luaPath = Utils.GetPackageRoot() + "/AssetBundle/lua/main.lua";
            if(!Utils.IsFileExist(luaPath)){
                Debug.LogError("LUA--虚拟机加载失败");
                return;
            }
            var luaStr = WeChatWASM.WX.GetFileSystemManager().ReadFileSync(luaPath).EncodeToString();
            luaenv = new LuaEnv();
            luaenv.DoString(luaStr);
            base.Awake();
#else
            if (Const.LOCAL)
            {
                luaPath = Application.dataPath + "/lua/main.lua";
            }
            else
            {
               luaPath = Utils.GetPersistentDataPath(true) + "/AssetBundle/lua/main.lua";
            }
            byte[] luaStr = File.ReadAllBytes(luaPath);
            luaenv = new LuaEnv();
            luaenv.DoString(luaStr);
            base.Awake();
#endif
    }
}
