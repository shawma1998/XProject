﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class NetManager : Singleton<NetManager>
{
    public Queue<BaseWebData> webRequests = new Queue<BaseWebData>();

    public float currTime = 0f;
    private float delay = 0.1f;
    private int maxQueue = 3;
    private bool canNext = true;

    public Action OnNetManagerOK;

    private void Start()
    {
        OnNetManagerOK?.Invoke();
    }

    public void AddRequest(string url, Action<byte[]> action , bool now = false)
    {

        var req = new UnityWeb(url, action);
        if (now)
        {
            StartCoroutine(RequestWebData(req));
        }
        else
        {
            webRequests.Enqueue(req);
        }
    }

    public void AddAssetBundleRequest(string url, Action<AssetBundle> action)
    {
        var is_ab = true;
        var req = new UnityABWeb(url, action);
        webRequests.Enqueue(req);
    }

    private IEnumerator RequestWebData(BaseWebData web)
    {
        var webRequest = web.request;
        yield return webRequest.SendWebRequest();
        if (webRequest.error != null)
        {
            Debug.LogError("-----------下载失败----------" + webRequest.error + " : " + webRequest.url);
            // this.AddRequest(web.url, web.action);
            web.AddRequest(web.url, web.action);
        }
        else
        {
            web.HandlerGetRequest();
        }
        webRequest.Dispose();
        canNext = true;
    }

    private void Update()
    {
        if (webRequests.Count == 0) return;
        if(canNext)
        {
            canNext = false;
            StartCoroutine(RequestWebData(webRequests.Dequeue())) ;
            currTime = Time.time;
        }
    }

}


public class BaseWebData
{
    public UnityWebRequest request;
    public int id;
    public string resName;
    public string url;
    public Action<byte[]> action;

    public virtual void HandlerGetRequest() { }
}


public class UnityWeb : BaseWebData
{

    public UnityWeb(string url, Action<byte[]> action)
    {
        this.action = action;
        this.url = url;
        request = UnityWebRequest.Get(this.url);
    }


    public void AddRequest(string url, Action<byte[]> action) {
         NetManager.Instance.AddRequest(url,action);
    }

    public UnityWeb(){}

    public override void HandlerGetRequest(){
        byte[] bytes = request.downloadHandler.data;
        action?.Invoke(bytes);
    }
}
public class UnityABWeb : BaseWebData
{
    public new Action<AssetBundle> action;
    public UnityABWeb(string url, Action<AssetBundle> action)
    {
        this.action = action;
        this.url = url;
        request = UnityWebRequestAssetBundle.GetAssetBundle(this.url);
    }



    public void AddRequest(string url, Action<AssetBundle> action) {
         NetManager.Instance.AddAssetBundleRequest(url,action);
    }

    public UnityABWeb(){}


    public override void HandlerGetRequest(){
        AssetBundle ab = (request.downloadHandler as DownloadHandlerAssetBundle).assetBundle;
        action?.Invoke(ab);
    }
}