﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XLua;

//用于LUA端的update逻辑
public class UpdateBeatManager : Singleton<UpdateBeatManager>
{
    public Dictionary<float, LuaFunction> functions = new Dictionary<float, LuaFunction>();
    // Update is called once per frame
    void Update()
    {
        foreach (var kv in functions)
        {
            if (kv.Value != null)
            {
                kv.Value.Call(Time.time , Time.deltaTime);
            }
        }
    }
    public void AddUpdateBeat(float id,LuaFunction func)
    {
        functions[id] = func;
    }

    public void RemoveUpdateBeat(float id)
    {
        functions[id] = null;
    }
}
