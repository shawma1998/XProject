﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CookieManager : Singleton<CookieManager>
{

    //储存cookie
    public void SetCookieContent(string content)
    {
        FileUtils.SetContentInPath(FileUtils.cookiePath,content);
    }

    //拿cookie
    public string GetCookieContent()
    {
       return FileUtils.GetContentInPath(FileUtils.cookiePath);
    }
}
