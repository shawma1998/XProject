﻿using UnityEngine;
using XLua;
using System.Collections.Generic;
using UnityEngine.UI;


/*
 * 管理界面预制实例的生成管理类
 * **/
public class PanelManager : Singleton<PanelManager>
{

    public Dictionary<string, Transform> dicUIParent = new Dictionary<string, Transform>();

    public void CreateView(string abName , string anRes , string layer , LuaFunction func)
    {
        //加载一个界面预制
        ResMgr.Instance.LoadRes(abName, anRes, null, 0 ,(obj) =>
        {
            var gameObject = Instantiate(obj) as GameObject;
            var trans = gameObject.transform;
            if (!dicUIParent.ContainsKey(layer))
            {
                dicUIParent[layer] = GameObject.Find("root/Canvas/" + layer).transform;
            }
            trans.SetParent(dicUIParent[layer]);

            trans.localScale = Vector3.one;
            trans.localPosition = Vector3.zero;
            //添加Canvas组件
            gameObject.AddComponent<Canvas>();
            gameObject.AddComponent<GraphicRaycaster>();

            if (func != null)
            {
                func.Call(gameObject);
            }
        });
    }

    public void CreateItem(string abName , string anRes , LuaFunction func)
    {
        //加载一个界面预制
        ResMgr.Instance.LoadRes(abName, anRes, null, 0 , (obj) =>
        {
            var gameObject = obj as GameObject;
            var trans = gameObject.transform;

            trans.localScale = Vector3.one;
            trans.localPosition = Vector3.zero;
            //添加Canvas组件
            if (!gameObject.GetComponent<CanvasRenderer>())
            {
                gameObject.AddComponent<CanvasRenderer>();
            }

            if (func != null)
            {
                func.Call(gameObject);
            }
        });
    }
}
