﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class Main : MonoBehaviour
{
    private HotUpdateView hotUpdateComponent;
    public bool localRes = true;

    // Start is called before the first frame update
    void Awake()
    {
#if UNITY_EDITOR
        Const.LOCAL = localRes;
#else
        Const.LOCAL = false;
#endif

        //加载网络模块
        var netMgr = this.gameObject.AddComponent<NetManager>();

        //for (int i = 0; i < 30; i++)
        //{
        //    NetManager.Instance.AddRequest("https://www.baidu.com/", (bytes) => { Debug.Log("OK"); });
        //}
        //检查热更资源
        netMgr.OnNetManagerOK += OnSelectCDNServer;
    }

    //选择CDN服务器
    void OnSelectCDNServer()
    {
        var parent = GameObject.Find("root/Canvas/UI").transform;
        var _prefab = Resources.Load<GameObject>("ServerSelectorView");
        var ServerSelectorUI = GameObject.Instantiate(_prefab);

        ServerSelectorUI.transform.parent = parent;
        ServerSelectorUI.transform.localPosition = Vector3.zero;
        ServerSelectorUI.transform.localScale = Vector3.one;

        var serverSelectorView = ServerSelectorUI.AddComponent<ServerSelectorView>();
        serverSelectorView.AddSelectCallBack(()=> {
            Destroy(ServerSelectorUI.gameObject);
            if(Utils.IsWechatMiniGame()){
                StartCheckResWEBGL();
            }else{
                StartCheckRes();
            }
        });
    }

    void StartCheckRes()
    {
        var parent = GameObject.Find("root/Canvas/UI").transform;
        var _prefab = Resources.Load<GameObject>("HotUpdateView");
        var hotUI = GameObject.Instantiate(_prefab);
        hotUI.transform.parent = parent;
        hotUI.transform.localPosition = Vector3.zero;
        hotUI.transform.localScale = Vector3.one;
        this.hotUpdateComponent = hotUI.AddComponent<HotUpdateView>();
        this.hotUpdateComponent.SetResCheckCallBack((success) => {
            if (success)
            {
                Debug.Log("加载最新资源成功");
                //解鸭全部的lua代码
                this.hotUpdateComponent.LoadAllLuaAsset((flag)=> {
                    Destroy(this.hotUpdateComponent.gameObject);
                    if (flag)
                    {
                        AddManager();
                    }
                    else
                    {
                        this.OnUpdateFail();
                    }
                });
            }
            else
            {
                this.OnUpdateFail();
            }
        });
    }



    void StartCheckResWEBGL()
    {
        var parent = GameObject.Find("root/Canvas/UI").transform;
        var _prefab = Resources.Load<GameObject>("HotUpdateView");
        var hotUI = GameObject.Instantiate(_prefab);
        hotUI.transform.parent = parent;
        hotUI.transform.localPosition = Vector3.zero;
        hotUI.transform.localScale = Vector3.one;
        var hotUpdateComponent = hotUI.AddComponent<HotUpdateView_WEBGNL>();
        //检查资源[下载检查lua的ab包]
        hotUpdateComponent.SetResCheckCallBack((success) => {
            if (success)
            {
                Debug.Log("加载最新资源成功");
                //解鸭全部的lua代码
                hotUpdateComponent.LoadAllLuaAsset((flag)=> {
                    Destroy(hotUpdateComponent.gameObject);
                    if (flag)
                    {
                        AddManager();
                    }
                    else
                    {
                        this.OnUpdateFail();
                    }
                });
            }
            else
            {
                this.OnUpdateFail();
            }
        });
    }

    void OnUpdateFail()
    {
        Application.Quit();
    }

    public void AddManager()
    {
        this.gameObject.AddComponent<ResMgr>();
        this.gameObject.AddComponent<PanelManager>();
        this.gameObject.AddComponent<UpdateBeatManager>();
        this.gameObject.AddComponent<CookieManager>();
        this.gameObject.AddComponent<SocketManager>();


        //最后再加载lua代码
        this.gameObject.AddComponent<LuaManger>();
    }

    private void OnApplicationQuit()
    {
        SocketManager.Instance.Dispose();
    }
}
