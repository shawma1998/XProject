﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class FileUtils
{
    public static RuntimePlatform platform = Application.platform;
    //常用路径

    private static string _cookiePath;
    public static string cookiePath
    {
        get
        {
            if (_cookiePath != null) return _cookiePath;
            if(platform == RuntimePlatform.Android)
            {
                _cookiePath = Application.persistentDataPath + "/Cookie/cookie.txt";
            }
            else
            {
                _cookiePath = Application.persistentDataPath + "/Cookie/cookie.txt";
            }
            return _cookiePath;
        }
    }

    public static string GetContentInPath(string path)
    {
        AutoCreateFile(path);
        var content = File.ReadAllText(path);
        return content;
    }

    public static void SetContentInPath(string path,string content)
    {
        AutoCreateFile(path);
        File.WriteAllText(path,content);
    }


    //自动创建文件
    public static void AutoCreateFile(string path)
    {
        if (!Directory.Exists(Path.GetDirectoryName(path)))
        {
            Directory.CreateDirectory(Path.GetDirectoryName(path));
        }
        if (!File.Exists(path))
        {
            File.Create(path).Close();
        }
    }
}
