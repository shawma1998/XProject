﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using UnityEngine;

public class Utils
{
    //获得某个文件的MD5值
    public static string GetMD5String(string fileName)
    {
        try
        {
            FileStream file = new FileStream(fileName, System.IO.FileMode.Open);
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] retVal = md5.ComputeHash(file);
            file.Close();
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < retVal.Length; i++)
            {
                sb.Append(retVal[i].ToString("x2"));
            }
            return sb.ToString();
        }
        catch (Exception ex)
        {
            throw new Exception("GetMD5HashFromFile() fail,error:" + ex.Message);
        }
    }
    //获得某个文件的MD5值
    public static string GetMD5String(byte[] bytes)
    {
        try
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] retVal = md5.ComputeHash(bytes);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < retVal.Length; i++)
            {
                sb.Append(retVal[i].ToString("x2"));
            }
            return sb.ToString();
        }
        catch (Exception ex)
        {
            throw new Exception("GetMD5HashFromFile() fail,error:" + ex.Message);
        }
    }


    /// <summary>
    /// 获得时间戳
    /// </summary>
    /// <returns></returns>
    public static string GetTimeStamp()
    {
        return new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds().ToString();
    }


    /// <summary>
    /// 根据平台获得本地资源路径
    /// </summary>
    /// <returns></returns>
    public static string GetLocalResLoadPath(bool append = false)
    {
        if (Const.LOCAL)
        {
            return Application.dataPath.Replace("/Assets", "/AssetBundle");
        }
        else
        {
            if (Const.PLATFORM == RuntimePlatform.Android)
            {
                return Application.persistentDataPath + "/AssetBundle";
            }


            return Application.persistentDataPath + "/AssetBundle"; 
        }
    }

    public static string GetWechatABPath()
    {
        return WeChatWASM.WX.env.USER_DATA_PATH + "/AssetBundle";
    }


    public static string GetPersistentDataPath(bool append = false)
    {
        if (Const.PLATFORM == RuntimePlatform.Android)
        {
            return Application.persistentDataPath;
        }

        return Application.persistentDataPath;
    }

    public static bool FindInString(string str , string parren)
    {
        return str.IndexOf(parren) != -1;
    }

    public static bool IsNull(GameObject obj)
    {
        if (obj == null)
        {
            return true;
        }
        else
        {
            return false;
        }
    }


    public static GameObject NewObj(GameObject obj)
    {
        return GameObject.Instantiate(obj);
    }

    
    public static Sprite ConvertObjectToSprite(Texture2D t2d)
    {
        return Sprite.Create(t2d, new Rect(0, 0, t2d.width, t2d.height), Vector2.zero);
    }


    //将绝对路径转换为相对路径
    public static string GetRelativePath(string fullPuth)
    {
        string mp = fullPuth;
        mp = mp.Substring(mp.IndexOf("Assets"));
        mp = mp.Replace('\\', '/');

        return mp;
    }


    public static string str2ASCII(string xmlStr)
    {
        byte[] array = System.Text.Encoding.ASCII.GetBytes(xmlStr);   //数组array为对应的ASCII数组       
        string ASCIIstr2 = null;
        for (int i = 0; i < array.Length; i++)
        {
            int asciicode = (int)(array[i]);
            ASCIIstr2 += Convert.ToString(asciicode);//字符串ASCIIstr2 为对应的ASCII字符串
        }

        return ASCIIstr2;
    }

    public static string Ascii2Str(byte[] buf)
    {
        return System.Text.Encoding.ASCII.GetString(buf);
    }


    public static bool GetIsLocal()
    {
#if WECHAT_MINI_GAME
        return false;
#endif
        return Const.LOCAL;
    }

    //检查是否是微信小游戏平台
    public static bool IsWechatMiniGame()
    {
#if WECHAT_MINI_GAME
    return true;      
#endif
        return Const.GAME_PACKAGE == GAME_PLATFORM.WEBGL_WECHAT_MINIGAME;
    }


    //检查是否是微信小游戏平台
    public static bool IsFileExist(string path)
    {
        if(Utils.IsWechatMiniGame())
        {
            Debug.LogError("FileExist====> "+ path + " @ " + WeChatWASM.WX.GetFileSystemManager().AccessSync(path));
            return WeChatWASM.WX.GetFileSystemManager().AccessSync(path) == "access:ok";
        }
        else
        {
            return File.Exists(path);
        }
    }

    public static string GetPackageRoot()
    {
#if WECHAT_MINI_GAME
        return WeChatWASM.WX.env.USER_DATA_PATH;
#else
        return GetPersistentDataPath();
#endif
    }



    //
    public static void WriteDataToPath(string path , byte[] bytes)
    {
#if WECHAT_MINI_GAME
        var direName = Path.GetDirectoryName(path);
        direName = direName.Replace(@"http:/", @"http://");
        //if (!Utils.IsFileExist(path)){
        //    WeChatWASM.WX.GetFileSystemManager().MkdirSync(direName, true);
        //}

        var result = WeChatWASM.WX.GetFileSystemManager().MkdirSync(direName, true);
        var write_result = WeChatWASM.WX.GetFileSystemManager().WriteFileSync(path,bytes);
        Debug.Log(direName + "   " + path + "  " + result + "  " + write_result);
#else
        if(!File.Exists(path)){
            File.Create(path);
        }
        File.WriteAllText(path,bytes.EncodeToString());
#endif
    }
}
