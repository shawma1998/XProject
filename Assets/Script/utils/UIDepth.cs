using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIDepth
{
    public static void SetDepth (GameObject obj , int num)
    {
        var _canvas = obj.GetComponent<Canvas>();
        _canvas.overrideSorting = true;
        _canvas.sortingOrder = num;
    }
}
