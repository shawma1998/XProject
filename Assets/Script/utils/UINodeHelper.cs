using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using XLua;
/*
 UI节点实力搜索工具类
 */
public class UINodeHelper 
{
    public static UnityEngine.Object[] GetUINodes(Transform transform , LuaTable nodes , string type)
    {
        UnityEngine.Object[] components = new UnityEngine.Object[nodes.Length];
        nodes.ForEach<int, string>((k, v) => {
            if(transform.Find(v) != null)
            {
                if (type == "GameObject")
                {
                    components[k - 1] = transform.Find(v).gameObject;
                }
                else if (type == "Image")
                {
                    components[k - 1] = transform.Find(v).GetComponent<Image>();
                }
                else if (type == "Transform")
                {
                    components[k - 1] = transform.Find(v);
                }
                else if (type == "Text")
                {
                    components[k - 1] = transform.Find(v).GetComponent<Text>();
                }
            }
        });
        return components;
    }
}
