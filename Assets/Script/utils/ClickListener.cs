using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using XLua;

public class ClickListener : MonoBehaviour , IPointerClickHandler
{
    private LuaFunction _func;
    public void AddClickFunc(LuaFunction func)
    {
        this._func = func;
    }

    public static void AddClickHandler(GameObject obj, LuaFunction func)
    {
        ClickListener _listener;
        _listener = obj.transform.GetComponent<ClickListener>() ?? obj.AddComponent<ClickListener>();
        _listener.AddClickFunc(func);
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (this._func != null)
        {
            this._func.Call(this.gameObject,eventData.position.x, eventData.position.y);
        } 
    }
}
