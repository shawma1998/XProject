using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using XLua;

public class ProtoFormator
{
    public static byte[] Format(int id , LuaTable table)
    {
        Dictionary<string, object> dic = new Dictionary<string, object>();
        table.ForEach<object, object>((k, v) => {
            dic[k.ToString()] = v.ToString();
        });
        dic["protoID"] = id.ToString();
        string json = JsonConvert.SerializeObject(dic);
        return json.GetBytes();
    }
    public static Dictionary<object,object> DeFormat(byte[] bytes)
    {
        Dictionary<object, object> dic = new Dictionary<object, object>();
        var json = bytes.EncodeToString();
        dic = JsonConvert.DeserializeObject<Dictionary<object, object>>(json);
        return dic;
    }
}
