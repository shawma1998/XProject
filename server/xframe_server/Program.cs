﻿using LuaInterface;
using System;
using System.IO;
using System.Threading;

namespace xframe_server
{
    public class Program
    {
        public static Lua serverLua;
        static void Main(string[] args)
        {
            serverLua = new Lua();        //创建一个lua解释器
            var luaStr = File.ReadAllText(@"Lua\main.lua");
            serverLua.DoString(luaStr);
            while (true)
            {
                Thread.Sleep(500);
                var command_lua = Console.ReadLine();
                serverLua.DoString(command_lua);
            }
        }
    }
}
