------------------基础功能---------------------
--SQL
require("module.sqlHelper.SQLHelper")
require("module.sqlHelper.sqlDoc")

--事件
require("module.event.EventManager")
require("module.event.EventName")

-------------------模块------------------
--登录模块
require("module.login.LoginController")
require("module.login.LoginModel")

--21点游戏
require("module.card.CardController")

--聊天模块
require("module.chat.ChatController")