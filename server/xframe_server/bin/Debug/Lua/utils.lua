__g_id = 0

function GetID()
    __g_id = __g_id + 1
    return __g_id
end

function Class(class)
    return luanet.import_type(class)
end

function Using(assembly)
    luanet.load_assembly(assembly)
end

function FormatTab( tab )
    for i, v in pairs(tab) do
        local num = tonumber(i)
        if num and not tab[num] then
            tab[num] = v
        end
    end
    return tab
end


function ToStringEx(value)
    if type(value)=='table' then
        return TableToStr(value)
    elseif type(value)=='string' then
        return "\'"..value.."\'"
    else
        return tostring(value)
    end
end


function ToStringEx(value,is_key)
    if type(value)=='table' then
        return TableToStr(value)
    elseif type(value)=='string' then
        if not is_key then
            return "\'"..value.."\'"
        end
        --return "\'"..value.."\'"
        return value
    else
        return tostring(value)
    end
end

function TableToStr(t)
    if t == nil then return "" end
    local retstr= "{"

    local i = 1
    for key,value in pairs(t) do
        local signal = ","
        if i==1 then
            signal = ""
        end

        if key == i then
            retstr = retstr..signal..ToStringEx(value)
        else
            if type(key)=='number' or type(key) == 'string' then
                retstr = retstr..signal..''..ToStringEx(key,true).."="..ToStringEx(value)
            else
                if type(key)=='userdata' then
                    retstr = retstr..signal.."*s"..TableToStr(getmetatable(key)).."*e".."="..ToStringEx(value)
                else
                    retstr = retstr..signal..key.."="..ToStringEx(value)
                end
            end
        end

        i = i+1
    end

    retstr = retstr.."}"
    return retstr
end

function StrToTable(str)
    if str == nil or type(str) ~= "string" or str == "" then
        return {}
    end

    return loadstring("return " .. str)()
end

function HandlerSQLReplaceString(str , func)
    local function FormatText(n)
        if func then
            return func(n)
        end
    end
    local str=string.gsub(str, "{#@(.+)@#}",FormatText)
    str = string.gsub(str,"{#@","")
    str = string.gsub(str,"@#}","")
    return str
end

--打印某个table
function PrintTable(table , level)
    local key = ""
    level = level or 1
    local indent = ""
    for i = 1, level do
        indent = indent.."  "
    end
    if key ~= "" then
        print(indent..key.." ".."=".." ".."{")
    else
        print(indent .. "{")
    end
    key = ""
    for k,v in pairs(table) do
        if type(v) == "table" then
            key = k
            PrintTable(v, level + 1)
        else
            local content = string.format("%s%s = %s", indent .. "  ",tostring(k), tostring(v))
            print(content)
        end
    end
    print(indent .. "}")
end



function ErrorCode(id)
    print("错误码-->> " .. id)
    return false
end


function GetTabSize(tab)
    local count = 0
    for i, v in pairs(tab) do
        count = count + 1
    end
    return count
end