Timer = Timer or {}
local TimerUtils = luanet.import_type("Timer.TimerUtils")
--delay  	//延迟
--times   	//次数
--period   	//间隔时间
function Timer.AddTimer(id,func,times,period,delay)
	delay = delay or 0
	period = period or 100
	times = times or 1
	TimerUtils.AddTimer(id,func,times,period,delay)
end

return Timer