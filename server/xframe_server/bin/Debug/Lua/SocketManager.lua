SocketManager = SocketManager or {}

SocketManager.proto_callback = {}
function SocketManager:BindSocket()
    local SocketServer_Class = Class("SocketUtil.SocketServer")
    self.server_socket = SocketServer_Class()
    self.server_socket:InitCallback(
            function(data,address)
                self:ConnectHandler(data,address)
            end,
            function(data,address)
                self:RequestHandler(data,address)
            end,
            function(data)
                self:DisConnectHandler(data)
            end
    )
    self.client_list = {}

    self.server_socket:Init()
    self.server_socket:OpenThread()
end


--收到客户端请求回调
function SocketManager:RequestHandler()
    local vo = self.server_socket.cacheReceive
    local protoTab = StrToTable(vo.json)
    print("LUA RECEIVE MSG -----  ",protoTab.protoID , vo.json)
    if protoTab.protoID then
        if self.proto_callback[protoTab.protoID] then
            local pvo = self.proto_callback[protoTab.protoID]
            pvo.func(pvo.that,protoTab.protoID,protoTab,vo.address)
        else
            print("协议ID未注册")
        end
    else
        print("没有协议id")
    end
end

--客户端连接回调
function SocketManager:ConnectHandler(data,address)
    print("LUA: CLIENT CONNECT --->> ")
    self.client_list[address] = data
end

--客户端连接回调
function SocketManager:DisConnectHandler(data)
    print("LUA: CLIENT DISCONNECT --->> ")
    local address = data
    self.client_list[address] = nil
    LoginModel:LoginOutPlayer(address)
end


-----------------------------

function SocketManager:Broadcast(address,data,id)
    if address and self.client_list[address] then
        self:SendAsync(address,data,id)
        return
    end
    for i, v in pairs(self.client_list) do
        self:SendAsync(i,data,id)
    end
end

function SocketManager:SendAsync(address , data , id)
    print("LUA: SERVER SEND --->> " ,address, data , id)
    local pid = id or data.protoID
    if not data.protoID then
        data.protoID = id
    end
    if not pid then error("没有指定协议id！！！！") return end
    local str = TableToStr(data).."<pt>"
    self.server_socket:AddDataSend(address,str)
end


---
function SocketManager:BindProto(id,func,that)
    self.proto_callback[id] = {that = that , func = func}
end

return SocketManager