CardController = CardController or {}
local insert = table.insert
local MAX_CARD = 52
function CardController:Init()
    self.room_list = {}
    --[[
    [room_id] = {
        player_card={
            [address]={1,2,34,5}        --玩家持有卡牌
        },
        pool = {1,2,34}     --剩余卡池
    }
    ]]
    self.card_pool = {}
    self:BindProto()
    self:BindEvent()
end

function CardController:BindProto()
    SocketManager:BindProto(20000,self.Handler20000,self)  --大厅数据
    SocketManager:BindProto(20001,self.Handler20001,self)  --创建房间
    SocketManager:BindProto(20002,self.Handler20002,self)  --加入房间
    SocketManager:BindProto(20003,self.Handler20003,self)  --房间数据更新
    SocketManager:BindProto(20004,self.Handler20004,self)  --删除房间
    SocketManager:BindProto(20005,self.Handler20005,self)  --等待房间
    SocketManager:BindProto(20006,self.Handler20006,self)  --准备
    SocketManager:BindProto(20007,self.Handler20007,self)  --游戏数据
    SocketManager:BindProto(20008,self.Handler20008,self)  --加牌
    SocketManager:BindProto(20009,self.Handler20009,self)  --完牌
    SocketManager:BindProto(20010,self.Handler20010,self)  --游戏结束
    SocketManager:BindProto(20011,self.Handler20011,self)  --退出房间
end

function CardController:BindEvent()
    local function login_out(address)
        local room = self:GetPlayerCurrRoom(address)
        if room then
            self:Handler20011(20011,nil,address)
        end
    end
    EventManager:Bind(EventName.PLAYER_LOGIN_OUT,login_out)
end

--大厅数据
function CardController:Handler20000(pid,data,address)
    local rooms = {}
    for i, v in pairs(self.room_list) do
        insert(rooms,{
            v.name,v.num,(v.max or 2),i
        })
    end
    SocketManager:Broadcast(address,{rooms},pid)
end

--创建房间
function CardController:Handler20001(pid,data,address)
    local name = data.name or data[1]
    local room = self:CreateRoom(name,address)
    local result = 1
    if not room or not next(room) then
        result = 0
    end
    self.room_list[room.id] = room
    self:Handler20000(20000,nil,nil)
    self:Handler20005(20005,{id = room.id},address)
    SocketManager:Broadcast(address,{result},pid)
end

--加入房间
function CardController:Handler20002(pid,data,address)
    local result = self:JoinRoom(data.id or data[1] , address)
    local code = result and 1 or 0
    SocketManager:Broadcast(address,{code},pid)
    if result then
        self:Handler20003(20003,{id = data[1]})
        local adds = self:GetRoomAllAddress(data[1])
        for i, v in ipairs(adds) do
            self:Handler20005(20005,{id = data[1]},v)
        end
    end
end

--房间数据更新
function CardController:Handler20003(pid,data,address)
    local room_id = data.id or data[1]
    local room = self.room_list[room_id]
    SocketManager:Broadcast(nil,{
        room.name , room.num , room.max , room.id
    },pid)
end

--删除房间20004
function CardController:Handler20004(pid,data,address)
    local room_id = data.id or data[1]
    self.room_list[room_id] = nil
    SocketManager:Broadcast(nil,{
        room_id
    },pid)
end

--房间玩家信息
function CardController:Handler20005(pid,data,address)
    local id = data.id or data[1]
    local player_list = self.room_list[id].player_list
    local result = {player_list = {}}
    for i, v in pairs(player_list) do
        local info = LoginModel:GetPlayerByAddress(v.address)
        insert(result.player_list,{
            info.name or "",
            tonumber(info.id),
            v.action or 1,
        })
    end
    SocketManager:Broadcast(address,result,pid)
end

--玩家准备or取消准备
function CardController:Handler20006(pid,data,address)
    local room = self:GetPlayerCurrRoom(address)
    if not room then
        ErrorCode(200061)
        return
    end
    if room.player_list[address] then
        room.player_list[address].action = data.action
    end
    local player_info = LoginModel:GetPlayerByAddress(address)
    local action_count = 0
    local max = room.max or 2
    for i, v in pairs(room.player_list) do
        if v.action == 1 then
            action_count = action_count + 1
        end
        SocketManager:Broadcast(i,{
            [1] = tonumber(player_info.id), --id
            [2] = data.action
        },pid)
    end
    if action_count >= max then
        self:Handler20007(20007,{id = room.id} , address)
    end
end

--游戏数据
function CardController:Handler20007(pid,data,address)
    local id = data.id
    local cards = self:CreateOrGetGameData(id).player_card
    local list = {player_cards = {}}
    for i, v in pairs(cards) do
        local vo = LoginModel:GetPlayerByAddress(i)
        insert(list.player_cards,{
            v,id = tonumber(vo.id)
        })
    end
    self:BroadcastRoom(id,list,20007)
end

--加牌
function CardController:Handler20008(pid,data,address)
    local player_id = LoginModel:GetAddressID(address)
    local r_id = self:GetPlayerCurrRoom(address).id
    local get_card = unpack(self:GetNumCardFromPool(r_id,1))

    local game_data = self.card_pool[r_id]
    if game_data and game_data.player_card and game_data.player_card[address] then
        insert(game_data.player_card[address],get_card)
    end

    self:BroadcastRoom(r_id,{
        player_id , get_card
    },20008)
end

--完牌
function CardController:Handler20009(pid,data,address)
    local player_id = LoginModel:GetPlayerByAddress(address).id
    local r_id = self:GetPlayerCurrRoom(address).id
    local game_data = self.card_pool[r_id]
    game_data.player_card[address]["state"] = 1
    self:BroadcastRoom(r_id,{
        tonumber(player_id)
    },pid)

    local game_end = true
    for i, v in pairs(game_data.player_card) do
        if v and (not v.state or v.state == 0) then
            game_end = false
            break
        end
    end
    if game_end then
        local mine , other = {} , {}
        for i, v in pairs(game_data.player_card) do
            if i == address then
                mine = v
            else
                other = v
            end
        end

        --游戏结束
        local room = self.room_list[r_id]
        if not room then return end
        for i, v in pairs(room.player_list) do
            if i == address then
                SocketManager:Broadcast(i, { mine,other },20010)
            else
                SocketManager:Broadcast(i, { other,mine },20010)
            end
            self:Handler20011(20011,nil,i)
        end
        self.card_pool[r_id] = nil
    end
end

--退出房间
function CardController:Handler20011(pid,data,address)
    local room = self:GetPlayerCurrRoom(address)
    if room and room.id and room.player_list then
        local r_id = room.id

        self:BroadcastRoom(r_id,{ 1 },20011)

        if room.player_list[address] then
            room.player_list[address] = nil
        end
        --没人了
        if not room.player_list or not room.player_list[address] or GetTabSize(room.player_list[address]) == 0 then
            self:Handler20004(20004,{id = r_id} , address)
        else
            self:Handler20003(20003,{id = r_id} , address)
        end
    else
        if room and room.id then
            self:BroadcastRoom(room.id,{ 0 },20011)
        end
    end
end








----------------------------------------逻辑--------------------------------------
--创建一个房间
function CardController:CreateRoom(name,address)
    local room = {}
    room = {
        name = name,
        num = 1,
        max = 2,
        time = os.time(),
        player_list = {
            [address] = {
                action = 0,
                address = address
            }
        },
        id = GetID()
    }
    return room
end

function CardController:JoinRoom(id , address)
    local room = self.room_list[id]
    if not room then
        return ErrorCode(1)     --房间不存在
    end
    local canjoin = true
    if room.player_list[address] then
        return ErrorCode(3)     --已有进房间了
    end
    if canjoin then
        room.player_list[address] = {
            action = 0,
            address = address
        }
    end
    return true
end

--获得玩家所在的房间
function CardController:GetPlayerCurrRoom(address)
    for i, v in pairs(self.room_list) do
        if v.player_list and v.player_list[address] then
            return v
        end
    end
    return false
end

--创建或拿一个游戏数据
function CardController:CreateOrGetGameData(r_id)
    if self.card_pool[r_id] and self.card_pool[r_id].player_card then
        return self.card_pool[r_id].player_card
    end
    local cards = self:GetRandomCards()
    self.card_pool[r_id] = {pool = cards}
    local player_list = self.room_list[r_id].player_list

    local player_card = {}
    for i, v in pairs(player_list) do
        player_card[i] = self:GetNumCardFromPool(r_id,2)
    end
    self.card_pool[r_id].player_card = player_card
    return self.card_pool[r_id]
end

--给游戏房间随机初始卡池
function CardController:GetRandomCards(max)
    max = max or MAX_CARD
    local cards = {}
    for i = 1, max do
        cards[i] = i
    end
    math.randomseed(os.time())
    math.random()

    local len = #cards
    local times = len * 5
    while times > 0 do
        local rSub = math.random(len)
        cards[1],cards[rSub] = cards[rSub],cards[1]
        times=times-1
    end
    return cards
end

--从房间卡池中拿数量的卡牌
function CardController:GetNumCardFromPool(r_id,num)
    local pool = self.card_pool[r_id].pool
    local cards = {}
    for i = 1, num do
        local _card = table.remove(pool,1)
        if _card then
            insert(cards,_card)
        end
    end
    return cards
end

--房间数据广播
function CardController:BroadcastRoom(r_id,data,pid)
    local room = self.room_list[r_id]
    if not room then return end
    for i, v in pairs(room.player_list) do
        SocketManager:Broadcast(i,data,pid)
    end
end

--获得房间所有地址
function CardController:GetRoomAllAddress(r_id)
    local list = {}
    local room = self.room_list[r_id]
    if not room then return end
    for i, v in pairs(room.player_list) do
        insert(list,i)
    end
    return list
end

CardController:Init()
return CardController