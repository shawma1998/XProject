EventManager = EventManager or {}
local insert = table.insert
__event_id = 0
function EventManager:Init()
    self.dic_event_list = {}
end

function EventManager:Bind(eventName,func)
    if not self.dic_event_list[eventName] then
        self.dic_event_list[eventName] = {}
    end
    local curr = __event_id
    if not self.dic_event_list[eventName][curr] then
        self.dic_event_list[eventName][curr] = func
        __event_id = __event_id + 1
        return curr
    end
    return false
end

function EventManager:Fire(eventName,...)
    if self.dic_event_list[eventName] then
        for i, func in pairs(self.dic_event_list[eventName]) do
            func(...)
        end
    end
end

function EventManager:UnBind(id)
    local del_i , del_i2
    if self.dic_event_list then
        for i, v in pairs(self.dic_event_list) do
            for i2, v2 in ipairs(v) do
                if i2 == id then
                    del_i = i del_i2 = i2
                    break
                end
            end
        end
        if del_i and del_i2 then
            self.dic_event_list[del_i][del_i2] = nil
            return id
        end
    end
    return false
end


EventManager:Init()
return EventManager