SQLHelper = SQLHelper or {}

function SQLHelper:Init()
    local SQLConnector_Class = Class("SQLUtil.SQLConnector")
    self.SQL = SQLConnector_Class()
    if self.SQL then
        self.SQL:InitConnect("xproject" , "127.0.0.1" , 3306 , "root" , "123456")
    end


end

function SQLHelper:Get(SQLName , func)
    if not self.SQL then
        return
    end
    if func then
        SQLName = HandlerSQLReplaceString(SQLName,function(index)
            if index then
                return func(index)
            end
        end)
        print("SQL --- >>>>  ",SQLName)
    end
    local data = self.SQL:CommandSQLQuery(SQLName)
    data = StrToTable(data)
    return data
end

function SQLHelper:Command(tab_name , SQLName)
    if not self.SQL then
        return
    end
    SQLName = string.gsub(SQLName,"@#TABNAME#@" , tab_name)
    local data = self.SQL:CommandSQLNonQuery(SQLName)
    return data
end

return SQLHelper