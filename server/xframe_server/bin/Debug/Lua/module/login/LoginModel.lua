LoginModel = LoginModel or {}
function LoginModel:Init()
    self.player_list = {}
end
--检查是否是合法用户
function LoginModel:CheckIsValidUser(user_name)
    local result
    result = SQLHelper:Get(SQL_DOC.SQL.CHECK_USER_VALID,function(arg)
        if arg == "USER_ID" then return user_name end
    end)
    return result
end

function LoginModel:GetPlayerByAddress(address)
    return self.player_list[address]
end

--玩家登录
function LoginModel:LoginPlayer(address,result)
    if not result then return false end
    if self.player_list[address] then
        return false
    end
    self.player_list[address] = {
        id = result.id,
        name = result.nick or "",
    }
    return true
end

--玩家登出
function LoginModel:LoginOutPlayer(address)
    if self.player_list[address] then
        self.player_list[address] = false
        --发送事件 玩家登出
        EventManager:Fire(EventName.PLAYER_LOGIN_OUT,address)
    end
end

function LoginModel:GetAddressID(address)
    local vo = self:GetPlayerByAddress(address)
    local id = vo and vo.id
    if id then
        return tonumber(id)
    end
    return -1
end

LoginModel:Init()
return LoginModel