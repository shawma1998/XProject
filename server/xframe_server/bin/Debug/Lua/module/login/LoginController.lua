LoginController = LoginController or {}


function LoginController:Init()
    self:BindProto()
end

function LoginController:BindProto()
    SocketManager:BindProto(10000,self.Handler10000)
    SocketManager:BindProto(10001,self.Handler10001)
end

function LoginController:Handler10000(pid,data,address)
    print(pid,data,address)
    local id = data.id
    local result = LoginModel:CheckIsValidUser(id)
    if result and result[1] then
        result = result[1]
        LoginModel:LoginPlayer(address,result)
        EventManager:Fire(EventName.PLAYER_LOGIN_IN,address)
        SocketManager:Broadcast(address,{
            name = result.nick,id = result.id,
            address = address,userName = result.userName
        },10002)
    end
    local respone = {}
    respone.code = next(result) and 1 or 0
    respone.protoID = 10000
    SocketManager:Broadcast(address,respone,10000)
end

function LoginController:Handler10001(pid,data,address)
    local info = LoginModel:GetPlayerByAddress(address)
    local respone = {}
    respone.code = 1
    if info and info.id then
        LoginModel:LoginOutPlayer(address)
        --EventManager:Fire(EventName.PLAYER_LOGIN_OUT,address)
    else
        respone.code = 0
    end
    SocketManager:Broadcast(address,respone,pid)
end

--function LoginController:Handler10002(pid,data,address)
--    local info = LoginModel:GetPlayerByAddress(address)
--    local respone = {}
--    respone.code = 1
--    if info and info.id then
--        LoginModel:LoginOutPlayer(address)
--        EventManager:Fire(EventName.PLAYER_LOGIN_OUT,address)
--    else
--        respone.code = 0
--    end
--    SocketManager:Broadcast(address,respone,pid)
--end

LoginController:Init()
return LoginController