﻿using LuaInterface;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLUtil
{
    public class SQLConnector
    {
        public MySqlConnection mySqlConnection;


        public MySqlConnection InitConnect(string dbName , string ip , int port , string userName , string pssww)
        {
            MySqlConnectionStringBuilder mysqlCSB = new MySqlConnectionStringBuilder();
            mysqlCSB.Database = dbName;   // 设置连接的数据库名
            mysqlCSB.Server = ip;  // 设置连接数据库的IP地址
            mysqlCSB.Port = (uint)port;           // MySql端口号
            mysqlCSB.UserID = userName;       // 设置登录数据库的账号
            mysqlCSB.Password = pssww;     // 设置登录数据库的密码
            //string mysqlCSB = "Database=school;Data Source=127.0.0.1;port=3306;User Id=root;Password=yang;";

            // 创建连接
            mySqlConnection = new MySqlConnection(mysqlCSB.ToString());

            // 打开连接(如果处于关闭状态才进行打开)
            if (mySqlConnection.State == ConnectionState.Closed)
            {
                mySqlConnection.Open();
            }
            return mySqlConnection;
        }

        //增删改查
        public void CommandSQLNonQuery(string mysqlInsert)
        {
            try
            {

                // 打开连接(如果处于关闭状态才进行打开)
                if (mySqlConnection.State == ConnectionState.Closed)
                {
                    mySqlConnection.Open();
                }

                // 创建用于实现MySQL语句的对象
                MySqlCommand mySqlCommand = new MySqlCommand(mysqlInsert, mySqlConnection);
                // 执行MySQL语句进行插入
                int result = mySqlCommand.ExecuteNonQuery();

                Console.WriteLine("数据库中受影响的行数({0})\n", result);

            }
            catch (Exception)
            {

            }
            finally
            {
                // 关闭连接
                mySqlConnection.Close();
            }
        }


        //增删改查
        public string CommandSQLQuery(string mysqlInsert)
        {
            try
            {

                // 打开连接(如果处于关闭状态才进行打开)
                if (mySqlConnection.State == ConnectionState.Closed)
                {
                    mySqlConnection.Open();
                }

                // 创建用于实现MySQL语句的对象
                MySqlCommand mySqlCommand = new MySqlCommand(mysqlInsert, mySqlConnection);
                // 执行MySQL语句进行插入
                MySqlDataReader mdr = mySqlCommand.ExecuteReader();
                string luaResult = "";


                while (mdr.Read())
                {
                    var kvs = "";
                    for (int i = 0; i < mdr.FieldCount; i++)
                    {
                        var keyName = mdr.GetName(i);
                        var val = mdr.GetValue(i).ToString();
                        kvs = kvs + $"{keyName} = '{val}',";
                    }

                    luaResult = luaResult + $"{{ {kvs} }}," + "\n";
                }

                luaResult = "{" + luaResult + "}";

                return luaResult;

            }
            catch (Exception)
            {
                return "";
            }
            finally
            {
                // 关闭连接
                mySqlConnection.Close();
            }
        }
    }
}
